from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from kulavi.urls import LOGIN_APP_URL
from . import views

urlpatterns = [
    url(r'^register/$', views.register_new_user, name='register-user'),
    url(r'^register/developer/$', views.register_new_user, name='register-developer'),
    url(r'^register/verify/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.email_verification, name='email-verification'),
    url(r'^update/profile/$', views.update_profile, name='update-profile'),
    url(r'^upload/game/$', views.upload_game, name='upload-game'),
    url(r'^update/game/([0-9]+)/$', views.update_game, name='update-game'),
    url(r'^delete/game/([0-9]+)/$', views.delete_game, name='delete-game'),

    url(r'^login/$', auth_views.login, {'template_name': 'login/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='logout'),
    url(r'^social/', include('social_django.urls', namespace='social')),

    # Check what kind of login was made and if profile should be made at this point
    url(r'^checklogin/$', views.check_login, name='check-login'),
    
    url(r'^password/change/$', auth_views.password_change, {'template_name' : 'login/password_management/password-change.html', 'post_change_redirect' : '/{0}/password/changed'.format(LOGIN_APP_URL)}, name='password-change'),
    url(r'^password/changed/$', auth_views.password_change_done, {'template_name' : 'login/password_management/password-change-done.html'}),

    #Urls for the password reseting using an email
    url(r'^password/reset/$', auth_views.password_reset, {'template_name': 'login/password_management/password-reset.html','email_template_name' : 'login/password_management/password-reset-email.html','post_reset_redirect' : '/{0}/password/reset/done'.format(LOGIN_APP_URL)}, name='password-reset'),
    url(r'^password/reset/done/$', auth_views.password_reset_done, {'template_name': 'login/password_management/password_reset_email_sent.html'}, name='password_reset_done'),
    url(r'^password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', auth_views.password_reset_confirm, {'template_name': 'login/password_management/password-reset.html'}, name='password_reset_confirm'),
    url(r'^password/reseted/$', auth_views.password_reset_complete, {'template_name': 'login/password_management/password_reseted.html'}, name='password_reset_complete'),
]