import datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import resolve
from django.urls import reverse
from login.forms import ProfileRegisterationForm, DeveloperRegisterationForm, GameForm, TagForm, ScreenshotForm
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from login.models import Profile, Developer
from gamebrowser.models import Game, Screenshot, MAXSCREENSHOTS
from django.db.utils import IntegrityError
from gamebrowser.views import home_page
from django.forms import modelformset_factory
from django.contrib.auth import logout

#For email validation
from django.template.loader import render_to_string
from django.http import Http404
from django.core.mail import send_mail
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes

def saveDeveloper(devform, user=None):
    '''Create or save the developer information'''
    if devform:
        if user:
            dev = devform.save(commit=False)
            dev.user=user
            dev.save()
        else:
            devform.save()

def saveGame(gameform, developer=None):
    ''' Create or save the game'''
    if developer:
        game = gameform.save(commit=False)
        game.developer = developer
        game.save()
        game.tags = gameform.cleaned_data['tags']
        game.save()
        return game
    else:
        return gameform.save()
     
def saveScreenshots(formset, game):
    for form in formset.cleaned_data:
        if 'image' in form.keys():
            if 'id' in form.keys() and not form['id']:
                shot = form['image']
                screenshot = Screenshot(game_id=game, image=shot)
            elif 'id' in form.keys():
                screenshot = Screenshot.objects.get(pk=form['id'].pk)
                screenshot.image = form['image']
            screenshot.save()

def isDeveloper(request):
    return hasattr(request.user, 'developer')

#Not actual view, but still uses request
def sent_verification_email(request, uid, token, email_to):
    context = {'uid':uid, 'token':token, 'site_name': 'Kulavi'}
    email_content = render_to_string('login/profile_management/register-validation-email.html', context, request=request)
    send_mail("Complete Kulavi registeration", email_content, "kulaviteam@kulavi.com", [email_to])

@login_required
def check_login(request):
    '''
    Check what kind of login was made and if profile should be made at this point
    '''
    profile = request.user.profile
    if not profile.email:
        try:
            profile.email = request.user.email
            profile.first_name = request.user.first_name
            profile.last_name = request.user.last_name
            profile.save()
        except:
            #If email is already in use, delete the user :)
            user = request.user
            logout(request)
            user.is_active = False
            user.delete()
            return render(request, "login/login.html", {"error_msg":"Login failed: the email you are using is already in use"})
    
    next_page = request.POST.get('next', '/') or home_page
    return redirect(next_page)

@csrf_protect                             
def register_new_user(request):
    ''' View to register a new user to the site'''
    #Check if registering normal user or developer
    developer = resolve(request.path_info).url_name == 'register-developer'
    if request.method == 'POST':
        userform = UserCreationForm(request.POST)
        profileform = ProfileRegisterationForm(request.POST, request.FILES)
        developerform = DeveloperRegisterationForm(request.POST) if developer else None

        #Check if the forms are valid (this also creates the cleaned_data attribute to forms)
        if userform.is_valid() and profileform.is_valid() and ((developerform and developerform.is_valid()) or not developerform):
            new_user = User.objects.create_user(
                username=userform.cleaned_data['username'],
                password=userform.cleaned_data['password1'])
            #Set inactive till the registeration is complete
            new_user.is_active = False
            # Save user's pk to uid and user itself as token for verification
            uid = urlsafe_base64_encode(force_bytes(new_user.pk))
            token = default_token_generator.make_token(new_user)
            # To save the profile with a correct user, another one must be made with an instance
            profileform2 = ProfileRegisterationForm(request.POST, instance=new_user.profile)
            saveDeveloper(developerform, new_user)
            pr = profileform2.save()
            pr.image = profileform.cleaned_data['image']
            pr.save()
            new_user.email = new_user.profile.email
            new_user.save()
            #Sent verification email
            sent_verification_email(request, uid, token, new_user.email)
            return render(request, 'login/profile_management/register-email-sent.html')
    else:
        userform = UserCreationForm()
        profileform = ProfileRegisterationForm()
        developerform = DeveloperRegisterationForm() if developer else None

    context = {'userform': userform, 'profileform' : profileform, 'developerform' : developerform, 'dev' : developer}
    return render(request, 'login/profile_management/register-user.html', context)

def email_verification(request, uidb64, token):
    ''' Verify the email'''
    if uidb64 and token:
        pk = urlsafe_base64_decode(uidb64)
        new_user = get_object_or_404(User, pk=pk)
        if default_token_generator.check_token(new_user, token) and new_user.is_active == 0:
            new_user.is_active = True
            new_user.save()                                
            return render(request, 'login/profile_management/register-complete.html')
    raise Http404("The verification link was invalid")

@login_required
@csrf_protect
def update_profile(request):
    '''View to update the profile information'''
    developer = isDeveloper(request)
    if request.method == 'POST':
        profileform = ProfileRegisterationForm(request.POST, request.FILES, instance=request.user.profile)
        developerform = DeveloperRegisterationForm(request.POST, instance=request.user.developer) if developer else None

        #Check if the forms are valid (this also creates the cleaned_data attribute to forms)
        if  profileform.is_valid() and ( (developerform and developerform.is_valid()) or not developer):
            saveDeveloper(developerform)
            profileform.save()
            request.user.email = request.user.profile.email
            request.user.save()
            return redirect(home_page)
    else:
        profileform = ProfileRegisterationForm(instance=request.user.profile)
        developerform = DeveloperRegisterationForm(instance=request.user.developer) if developer else None

    context = {'profileform' : profileform, 'developerform' : developerform, 'dev' : developer}
    return render(request, 'login/profile_management/update-profile.html', context)

@login_required
@csrf_protect
def upload_game(request):
    ''' Upload a new game to the web shop '''
    #Developer status is required for uploading games
    if not isDeveloper(request):
        return render(request, 'login/game_upload/upload_game.html', {'dev' : False})

    screenshotFormSet = modelformset_factory(Screenshot, ScreenshotForm, extra=MAXSCREENSHOTS)
    if request.method == 'POST':
        gameform = GameForm(request.POST, request.FILES)
        screenshotSet = screenshotFormSet(request.POST, request.FILES, queryset=Screenshot.objects.none())
        if gameform.is_valid() and screenshotSet.is_valid():
            game = saveGame(gameform, request.user.developer)
            saveScreenshots(screenshotSet, game)

            #Return back to caller page or home page
            next_page = request.POST.get('next', '/') or home_page
            return redirect(next_page)
    else:
        gameform = GameForm()
        screenshotSet = screenshotFormSet(queryset=Screenshot.objects.none())
    
    context = {'dev' : True, 'formid':'gameform', 'gameform' : gameform, 'formset':screenshotSet, 'title' : "Upload a new game", 'pagename':"Upload a new game", 'buttontext':"Upload"}
    return render(request, 'login/game_upload/upload_game.html', context)

@login_required
@csrf_protect
def update_game(request, gamepk=-1):
    ''' Upload a new game to the web shop '''
    #Developer status is required for updating games
    if not isDeveloper(request):
        return render(request, 'login/game_upload/upload_game.html', {'dev' : False})

    screenshotFormSet = modelformset_factory(Screenshot, ScreenshotForm, extra=MAXSCREENSHOTS)
    if request.method == 'POST':
        game = get_object_or_404(Game, developer=request.user.developer, pk=gamepk);
        gameform = GameForm(request.POST, request.FILES, instance=game)
        screenshotSet = screenshotFormSet(request.POST, request.FILES, queryset=Screenshot.objects.filter(game_id=gamepk))
        if gameform.is_valid() and screenshotSet.is_valid():
            saveGame(gameform)
            saveScreenshots(screenshotSet, game)
            next_page = request.POST.get('next', '/') or home_page
            return redirect(next_page)
    else:
        game = get_object_or_404(Game, developer=request.user.developer, pk=gamepk)
        gameform = GameForm(instance=game)
        screenshotSet = screenshotFormSet(queryset=Screenshot.objects.filter(game_id=gamepk))
    context = {'dev' : True, 'formid':'gameform', 'gameform' : gameform, 'formset':screenshotSet, 'game': game, 'title' : "Update {0}".format(game.name), 'pagename':"Update {0}".format(game.name), 'buttontext':"Update"}
    return render(request, 'login/game_upload/upload_game.html', context)

@login_required
@csrf_protect
def delete_game(request, gamepk=-1):
    #Developer status is required for deleting games
    if not isDeveloper(request):
        return render(request, 'login/game_upload/upload_game.html', {'dev' : False})
    game = get_object_or_404(Game, developer=request.user.developer, pk=gamepk)
    game.delete()
    return redirect(reverse('developed-games'))