import datetime

from login.forms import ProfileRegisterationForm, DeveloperRegisterationForm
from django.contrib.auth.forms import UserCreationForm
from django.test import TestCase, Client
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from .models import Profile, Developer, DEFAULTPIC, image_path
from django.contrib.auth.models import User



class SimpleTest(TestCase):
    #Test login app

    def setUp(self):
        self.client = Client()
        self.valid_user_data = {'username': 'testuser', 'password1': 'salasana', 'password2': 'salasana'}
        self.valid_profile_data = {'birthdate_month': '3', 'phone': '050050050',  'country': 'FI', 'email': 'testuser@gmail.com', 'birthdate_day': '10', 'address2': 'A 23', 'first_name': 'Test', 'last_name': 'User', 'zip': '00670', 'birthdate_year': '1946', 'address1': 'Pakilantie 5', 'city': 'Helsinki'}
        self.faulted_profile_data = {'birthdate_month': '13', 'phone': '0050050231232323232323',  'country': 'fi', 'email': 'testusergmail.com', 'birthdate_day': '32', 'zip': '123123123123123123123', 'birthdate_year': '2010'}
        self.valid_developer_data = {'developer_name' : 'testdev', 'company' : 'testcompany', 'bank_account' : 'FI80123321123321'}
        
        #run tests that are needed with other tests
        self.initTests()

    def initTests(self):
        #Create new user and test User creation form (no need to test, but writing tests is fun!)
        user_form = UserCreationForm(data = self.valid_user_data)
        self.assertTrue(user_form.is_valid(), "Valid user form wasn't valid")
        self.valid_user_data['password2'] = 'different'
        user_form_faulted = UserCreationForm(data = self.valid_user_data)
        self.assertFalse(user_form_faulted.is_valid(), "Password validation failed")
        self.user = User.objects.create_user(username = user_form.cleaned_data['username'], password= user_form.cleaned_data['password1'])

        profile_form = ProfileRegisterationForm(data = self.valid_profile_data, instance=self.user.profile)
        self.assertTrue(profile_form.is_valid(), "Valid profile form wasn't valid")
        #Raises an error if invalid
        profile_form.save()
        #Now self.user.profile is usable

    def testProfileForm(self):  
        #Profile form and model        
        invalid_profile_form = ProfileRegisterationForm(data = self.valid_profile_data)
        self.assertRaises(ValueError, invalid_profile_form.save, "Profile form with no user was saved")
  
        #Test all invalid profile fields
        for key, val in self.faulted_profile_data.items():
            tempval = self.valid_profile_data[key]
            self.valid_profile_data[key] = val
            invalid_profile_form = ProfileRegisterationForm(data = self.valid_profile_data, instance=self.user.profile) 
            self.assertFalse(invalid_profile_form.is_valid(), "Profile form with an invalid field '{0}' was valid".format(key))
            self.valid_profile_data[key] = tempval
        
        #Tests that saved profile has all the fields that were assigned
        for key in self.valid_profile_data:
            if "birth" not in key:
                self.assertEqual(getattr(self.user.profile, key), self.valid_profile_data[key], "Profile field '{0}' wasn't saved correctly".format(key))

        #Test that the latitude and longitude were created
        self.assertEqual(self.user.profile.latitude, 60.167, "Profile latitude wasn't saved correctly: expected {0}, got {1}".format(60.167, self.user.profile.latitude))
        self.assertEqual(self.user.profile.longitude, 24.943, "Profile latitude wasn't saved correctly: expected {0}, got {1}".format(24.943, self.user.profile.longitude))

    def testDevForm(self):
        dev_form = DeveloperRegisterationForm(data = self.valid_developer_data)
        self.assertTrue(dev_form.is_valid(), "Valid developer form wasn't valid")

        #Raises an error if fails
        dev = Developer(user=self.user, company = dev_form.cleaned_data['company'], developer_name = dev_form.cleaned_data['developer_name'], bank_account = dev_form.cleaned_data['bank_account'])
        self.assertEqual(dev.user.profile.email, self.user.profile.email, "The developer wasn't saved well")
        dev.full_clean()