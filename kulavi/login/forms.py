import datetime
import collections
from PIL import Image
from io import BytesIO

from django import forms
from django.forms import ModelForm, ModelMultipleChoiceField
from django.contrib.auth.models import User
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import UsernameField
from login.models import Profile, Developer
from gamebrowser.models import Game, Screenshot, Tag

#Constant values needed in forms
AGELIMIT = 10
OLDESTYEAR  = 1940
IMAGESIZELIMIT = 5242880

PROFILEPICSIZE = (140,120)
GAMEPICSIZE = (460,215)
SCREENSHOTPICSIZE = (1366,768)


def resizeImage(imagefile, target_size):
    '''Resizes the image to the wanted size'''
    if imagefile and type(imagefile) != str:
        if imagefile.size > IMAGESIZELIMIT:
            raise ValidationError('There file size exceeds the file size limit')
        try:
            im = Image.open(imagefile)
            origFormat = im.format
            im = cropImage(im, target_size)
            if im.size > target_size:
                #Resize the image to have at maximum size of target_size
                im.thumbnail(target_size, Image.ANTIALIAS)
            im_io = BytesIO()
            im.save(im_io, format=origFormat)
            im_io.seek(0)
            imagefile.file = im_io
        except:
            raise ValidationError('There was a problem with one of the images you were trying to upload!')

def cropImage(im, target_size):
    '''Crops the image to have the approximately the wanted scale'''
    width, height = im.size
    origScale = 1.0*width/height
    targetScale = 1.0*target_size[0]/target_size[1]
    
    if origScale < targetScale:
        #The height is bigger than wanted
        yOffset = (targetScale*height-width)/targetScale
        im=im.crop((0, yOffset/2, width, height-yOffset/2))
    elif origScale > targetScale:
        #The width is bigger than wanted
        xOffset = width-targetScale*height
        im = im.crop((xOffset/2, 0, width-xOffset/2, height))
    return im


class ProfileRegisterationForm(ModelForm):
    ''' Form for registering new user's profile (for User, the predefined UserCreationForm is used)'''
    class Meta:
        model = Profile
        exclude = ('user','latitude', 'longitude')
        widgets = {
            'birthdate' : forms.SelectDateWidget(years = range(OLDESTYEAR, datetime.date.today().year - AGELIMIT)),
            }
    
    def clean_birthdate(self):
        #Check that user is over <AGELIMIT> years old and that the date is valid
        today = datetime.date.today()       
        try:
            birthdate = self.cleaned_data.get("birthdate")
            if birthdate and birthdate > today.replace(year = today.year - AGELIMIT):
                raise ValidationError('The user must be over 10 years old and the birthdate must be a valid date.')
        except ValueError:
            #Leap year
            if birthdate > today.replace(year = today.year - AGELIMIT, day = today.day -1):
                raise ValidationError('The user must be over 10 years old and the birthdate must be a valid date.')
        return birthdate

    def clean_image(self):
        '''Crops and resizes the image '''
        image = self.cleaned_data.get("image")
        resizeImage(image, PROFILEPICSIZE)
        return image

class DeveloperRegisterationForm(ModelForm):
    ''' Form for registering new developer profile in addition of the user profile '''
    class Meta:
        model = Developer
        exclude = ('user',)

# Have to inherit a class and override method to get game tag names to the list
class TagChooser(ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name

class GameForm(ModelForm):
    ''' Form for uploading and updating games '''
    tags = TagChooser(queryset=Tag.objects.all().order_by('name'), label="Tags (select multiple while holding CTRL):");

    class Meta:
        model = Game
        exclude = ('developer', 'release_date', 'update_date',)

    def clean_thumbnail(self):
        '''Crops and resizes the image '''
        image = self.cleaned_data.get("thumbnail")
        resizeImage(image, GAMEPICSIZE)
        return image

class ScreenshotForm(ModelForm):
    ''' Form for uploading screenshots '''
    class Meta:
        model = Screenshot
        exclude = ('game_id',)
        widgets = {'image' : forms.FileInput(attrs={'class':'game_screenshots'})}
    
    def clean_image(self):
        '''Crops and resizes the image '''
        image = self.cleaned_data.get("image")
        resizeImage(image, SCREENSHOTPICSIZE)
        return image

class TagForm(ModelForm):
    ''' Form for uploading and updating tags '''
    class Meta:
        model = Tag
        exclude = ('',)