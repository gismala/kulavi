from django.test import TestCase
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from .models import Game, Tag, Screenshot, OwnedGame, ShoppingCart
from login.models import User, Developer
from login.forms import GameForm, ScreenshotForm, TagForm

# Create your tests here.

class GameTestCase(TestCase):
    def setUp(self):
        self.valid_tag_names = ['RPG', 'Action', 'Puzzle']

        self.valid_tags = []

        for tag in self.valid_tag_names:
            t = Tag(name=tag)
            t.full_clean()
            t.save()
            self.valid_tags.append(t)

        self.valid_game_fields = {'name': 'Valid Game: Director\'s Cut',
            'url': 'http://www.validsoft.com/valid_game/play.php',
            'price': 49.99,
            'discount': 50,
            'short_description': 'Wowie-zowie! What a valid game!',
            'long_description': 'Perhaps the most valid game you\'ll ever see, Valid Game sets out to push the industry\'s standards on how valid a game can be.',
            'thumbnail': 'defaultpics/default_thumbnail.png',
            'tags': self.valid_tags
            }

        self.invalid_game_fields = {'name': ['Valid Game: Director\'s Cut', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
            'url': ['http://www.validsoft.com/valid_game/play.php', 'www.validsoft.com/valid_game/play.php', 'Didn\'t expect this kind of URL, did you mate?'],
            'price': [1234567890, 99.999, 123456789.5, 'cheapo'],
            'discount': [-10, 150, 50.5, 'half price'],
            'short_description': ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'],
            'long_description': [],
            'thumbnail': [],
            'tags': [[]]
            }

        #Create dummy developer
        self.devuser = User.objects.create_user(username = 'dev', password = 'salasana')
        self.dev = Developer(user = self.devuser, developer_name = 'dev', company = 'com', bank_account = '123')
        self.dev.save()

    def test_tag(self):
        #name too long
        t = Tag(name = 'qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiopq')
        self.assertRaises(ValidationError, t.full_clean, 'Too long tag was accepted')
        #Duplicate name
        t = Tag(name = 'Action')
        self.assertRaises(ValidationError, t.full_clean, 'Duplicate tag was accepted')

    def test_game(self):
        #Test valid game fields
        valid_game_form = GameForm(data = self.valid_game_fields)
        self.assertTrue(valid_game_form.is_valid(), 'Valid game form wasn\'t valid')

        #Create valid game
        valid_game = Game(name = valid_game_form.cleaned_data['name'],
            url = valid_game_form.cleaned_data['url'],
            price = valid_game_form.cleaned_data['price'],
            discount = valid_game_form.cleaned_data['discount'],
            short_description = valid_game_form.cleaned_data['short_description'],
            long_description = valid_game_form.cleaned_data['long_description'],
            thumbnail = valid_game_form.cleaned_data['thumbnail'],
            developer = self.dev)
        valid_game.save()
        valid_game.tags = valid_game_form.cleaned_data['tags']
        valid_game.save()

        #Test that saved game has all the fields that were assigned
        for key in self.valid_game_fields:
            if key == 'price':
                self.assertEqual(float(getattr(valid_game, key)), self.valid_game_fields[key], 'Game field \'{0}\' wasn\'t saved correctly'.format(key))
            elif key != 'tags':
                self.assertEqual(getattr(valid_game, key), self.valid_game_fields[key], 'Game field \'{0}\' wasn\'t saved correctly'.format(key))
        self.assertSetEqual(set([t.name for t in valid_game.tags.all()]), set(self.valid_tag_names), 'Game tags weren\'t saved correctly')

        #Update game, check that update date is updated
        valid_game.name = 'Valid Game 2: Electric Boogaloo'
        valid_game.save()
        self.assertGreater(valid_game.update_date, valid_game.release_date, 'Update date wasn\'t updated properly')
        #Change it back for the next step
        valid_game.name = 'Valid Game: Director\'s Cut'
        valid_game.save()

        #Test all invalid game fields
        #Change name and url in valid_fields because they are no longer valid
        self.valid_game_fields['name'] = 'Another Game'
        self.valid_game_fields['url'] = 'http://www.anothersoft.com/another_game/play.php'

        for key, val in self.invalid_game_fields.items():
            tempval = self.valid_game_fields[key]
            for v in val:
                self.valid_game_fields[key] = v
                invalid_game_form = GameForm(data = self.valid_game_fields)
                self.assertFalse(invalid_game_form.is_valid(), 'Game form with the invalid value \'{0}\' in the field \'{1}\' was valid'.format(v, key))
            self.valid_game_fields[key] = tempval
