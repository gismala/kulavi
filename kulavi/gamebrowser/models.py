from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator

from django.db.models import Avg, Count, Sum

DEFAULTPIC = 'defaultpics/default_thumbnail.png'
#Maximum amount of screenshots per game
MAXSCREENSHOTS = 5

#instance.id will not have a value until the Game instance is saved.
#Make sure it has been saved before letting the user upload screenshots or thumbnails.
def image_path(instance, filename):
    return 'screenshots/game_{0}/{1}'.format(instance.game_id.id if hasattr(instance, 'game_id') else instance.id, filename)

class Tag(models.Model):
    name = models.CharField(max_length=50, unique=True)

    class Meta:
        ordering = ['name']

class Game(models.Model):
    name = models.CharField(max_length=250, unique=True, help_text="Game name")
    url = models.URLField(unique=True , help_text="http://www.example.com/game.html")
    price = models.DecimalField(max_digits=9, decimal_places=2, help_text="Price in euros")
    discount = models.PositiveSmallIntegerField(default=0, help_text="How many % of discount", validators=[MaxValueValidator(100)])
    developer = models.ForeignKey('login.Developer', on_delete=models.CASCADE, related_name='games')
    short_description = models.CharField(max_length=250, blank=True, help_text="Write a short description of your game")
    long_description = models.TextField(blank=True, help_text="Write here a longer description of your game")
    release_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag, related_name='games')
    thumbnail = models.ImageField(upload_to=image_path, default=DEFAULTPIC)

    def payments_count(self):
        return self.payments.count()

    def payments_sum(self):
        return sum(self.payments.values_list('amount', flat=True))

    def total_playtime(self):
        return sum(self.owners.values_list('time_played', flat=True))

    def rating(self):
        total = self.owners.exclude(rating__isnull=True).count()
        if total > 0:
            return sum(self.owners.values_list('rating', flat=True)) / total
        return None

class OwnedGame(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owned_games')
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='owners')
    time_played = models.PositiveIntegerField(default=0)
    score = models.PositiveIntegerField(default=0)
    rating = models.PositiveSmallIntegerField(blank=True, null=True)
    state = models.TextField(blank=True)

class Screenshot(models.Model):
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='screenshots')
    image = models.ImageField(upload_to=image_path)

class ShoppingCart(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='shopping_cart')
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE, related_name='shopping_cart')

    def __str__(self):
        return "{0}\n : {1}".format(self.user_id.name, self.game_id.name)
