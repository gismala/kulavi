from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^home/$', views.home_page, name='home'),
    url(r'^search/$', views.game_list, name='game-list'),
    url(r'^cart/$', views.game_list, name='cart'),
    url(r'^collection/$', views.game_list, name='owned-games'),
    url(r'^developer/$', views.game_list, name='developed-games'),
    url(r'^view/([0-9]+)/$', views.game_view, name='game-view'),
    url(r'^cart/action/$', views.change_cart, name='change-cart'),
    url(r'^playgame/([0-9]+)/$', views.play_game, name='play-game'),
    url(r'^update/([0-9]+)/$', views.update, name='update-game-info'),
    url(r'^addtime/([0-9]+)/$', views.add_time, name='add-playing-time'),
    url(r'^rate/([0-9]+)/$', views.submit_rating, name='rate-game'),
]
