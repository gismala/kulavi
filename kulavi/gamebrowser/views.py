from django.shortcuts import render, get_object_or_404, redirect
from django.db.models import Q, Avg, Count, Sum
from django.http import HttpResponse, Http404, JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import resolve
from django.urls import reverse

import datetime
from math import ceil
import json

from .models import Game, Tag, Screenshot, OwnedGame, ShoppingCart
from login.models import User, Developer

# Create your views here.
def home_page(request):
    newest_games = Game.objects.order_by('-release_date')[0:5]
    top_rated_games = Game.objects.annotate(avg_rating = Avg('owners__rating')).filter(avg_rating__gt = 0).order_by('-avg_rating')[0:5]
    total_most_played_games = Game.objects.annotate(total_playtime = Sum('owners__time_played')).filter(total_playtime__gt = 0).order_by('-total_playtime')[0:5]
    if request.user.is_authenticated():
        personal_most_played_games = Game.objects.filter(owners__user_id = request.user).filter(owners__time_played__gt = 0).distinct().order_by('-owners__time_played')[0:5]
    else:
        personal_most_played_games = []
    most_bought_games_month = Game.objects.filter(payments__payment_time__gte = datetime.date.today() - datetime.timedelta(days=30)).annotate(payments_month = Count('payments')).order_by('-payments_month')[0:5]

    context = {
        'all_tags': Tag.objects.all(),
        'newest_games': newest_games,
        'top_rated_games': top_rated_games,
        'total_most_played_games': total_most_played_games,
        'personal_most_played_games': personal_most_played_games,
        'most_bought_games_month': most_bought_games_month
    }
    return render(request, 'gamebrowser/home_page.html', context)

def game_list(request):
    #Displays a page of the list of games, optionally filtered by tag and search
    #words. The search applies to name, short and long descriptions and the
    #results are combined and ordered by release date.

    #Detect the URL pattern used to reach this view. Reuse the view with slight
    #modifications for different URLs.
    mode = resolve(request.path_info).url_name
    #Check authentication required by mode.
    if mode in ['cart', 'owned-games', 'developed-games']:
        if not request.user.is_authenticated():
            return redirect(reverse('login')+'?next='+request.path)
        if mode == 'developed-games' and not hasattr(request.user, 'developer'):
            return redirect(reverse('login')+'?next='+request.path)

    #Retrieve URL query strings and assign default values where necessary
    query_tag = request.GET.get("query_tag")
    if not query_tag:
        query_tag = ""
    query_text = request.GET.get("query_text")
    if not query_text:
        query_text = ""
    page_number = request.GET.get("page_number")
    if page_number:
        try:
            page_number = int(page_number)
        except ValueError:
            page_number = 1
    else:
        page_number = 1
    per_page = request.GET.get("per_page")
    if per_page:
        try:
            per_page = int(per_page)
        except ValueError:
            per_page = 10
    else:
        per_page = 10
    order_by = request.GET.get("order_by")
    if not order_by or order_by not in ('name', 'release_date', 'update_date', 'price'):
        order_by = 'release_date'
    order_dir = request.GET.get("order_dir")
    if not order_dir:
        if order_by == 'name':
            order_dir = 'asc'
        elif order_by == 'release_date':
            order_dir = 'desc'
        elif order_by == 'update_date':
            order_dir = 'desc'
        elif order_by == 'price':
            order_dir = 'asc'
    if order_dir not in ('asc', 'desc'):
        order_dir = 'asc'

    #Mode filtering
    if mode=='cart':
        matching_games = Game.objects.filter(shopping_cart__in=request.user.shopping_cart.all())
    elif mode=='owned-games':
        matching_games = Game.objects.filter(owners__in=request.user.owned_games.all())
    elif mode=='developed-games' and hasattr(request.user, 'developer'):
        matching_games = Game.objects.filter(developer=request.user.developer)
    else:
        matching_games = Game.objects.all()
    num_total = matching_games.count()

    #Tag matching
    if query_tag != "":
        matching_tag = get_object_or_404(Tag, name=query_tag)
        matching_games = matching_tag.games.filter(tags=matching_tag)

    #Search matching
    if query_text != "":
        query_parts = query_text.split(' ')

        #The line below should find all games where EVERY search term is found
        #in ONE of the fields: name, short_description, long_description,
        #tags.name, developer.developer_name
        #The \ is just there to put a line break into the code.
        for q in query_parts:
            matching_games = matching_games.filter( \
            Q(name__icontains=q) | \
            Q(short_description__icontains=q) | \
            Q(long_description__icontains=q) | \
            Q(tags__name__icontains=q) | \
            Q(developer__developer_name__icontains=q)).distinct()

    #Add additional fields to queryset
    matching_games = matching_games.extra(select={'discounted_price': 'price - price * (CAST(discount AS REAL) / 100)'})

    #Ordering
    if order_dir == 'asc':
        if order_by == 'price':
            matching_games = matching_games.extra(order_by = ['discounted_price'])
        else:
            matching_games = matching_games.order_by(order_by)
    elif order_dir == 'desc':
        if order_by == 'price':
            matching_games = matching_games.extra(order_by = ['-discounted_price'])
        else:
            matching_games = matching_games.order_by('-'+order_by)

    num_results = matching_games.count()

    #Pagination
    #Get a list of pages to be displayed in page list. They will be the five
    #numbers surrounding the current page: i-2, i-1, i, i+1, i+2
    first_page = 1
    last_page = ceil(num_results / per_page)
    displayed_pages = list(range(max(first_page, page_number-2),min(last_page, page_number+2)+1))
    if displayed_pages != []:
        uncollapsed_pages = [displayed_pages[0]-1] + displayed_pages + [displayed_pages[-1]+1]
    else:
        uncollapsed_pages = []

    starting = (page_number-1)*per_page
    matching_games = matching_games[starting:starting+per_page]

    num_displayed = matching_games.count()

    #Is the game already in shopping cart or owned by the user?
    if request.user.is_authenticated():
        for game in matching_games:
            game.in_cart = ShoppingCart.objects.filter(user_id=request.user, game_id=game).exists()
            game.owned = OwnedGame.objects.filter(user_id=request.user, game_id=game).exists()
            game.developed = hasattr(request.user, 'developer') and game.developer == request.user.developer
    else:
        for game in matching_games:
            game.in_cart = False
            game.owned = False

    context = {
        'mode': mode,
        'first_page': first_page,
        'last_page': last_page,
        'current_page': page_number,
        'uncollapsed_pages': uncollapsed_pages,
        'displayed_pages': displayed_pages,
        'all_tags': Tag.objects.all(),
        'displayed_games': matching_games,
        'query_text': query_text,
        'query_text_url': query_text.replace(' ','+'),
        'query_tag': query_tag,
        'per_page': per_page,
        'order_by': order_by,
        'order_dir': order_dir,
        'num_displayed': num_displayed,
        'num_results': num_results,
        'num_total': num_total
    }
    if request.is_ajax():
        #Load only the list when called with AJAX
        return render(request, 'gamebrowser/game_list_list.html', context)
    else:
        return render(request, 'gamebrowser/game_list.html', context)

def game_view(request, game_id):
    #Display information about the selected game. This includes all possible
    #information. Thus, additional information is fetched into the context.

    matching_game = get_object_or_404(Game, id=game_id)

    #Finding rating and high scores from OwnedGame
    #Player count in template like game.owners.count
    avg_rating = matching_game.owners.aggregate(Avg('rating'))
    rates = OwnedGame.objects.filter(game_id=matching_game, rating__gt=0).count()
    top_players = matching_game.owners.order_by('-score')[0:10]

    #Finding top 5 similar games
    tags = matching_game.tags.all()
    tag_matches = Game.objects.filter(tags__in=tags).annotate(Count('name')).exclude(pk=matching_game.pk)
    similar_games = tag_matches.order_by('-name__count')[0:4]

    discounted_price = matching_game.price - matching_game.price * matching_game.discount / 100

    #Is the game already in shopping cart or owned by the user?
    user_rating = 0
    hours = 0
    minutes = 0
    if request.user.is_authenticated():
        in_cart = ShoppingCart.objects.filter(user_id=request.user, game_id=matching_game).exists()
        owned = OwnedGame.objects.filter(user_id=request.user, game_id=matching_game).exists()
        developed = hasattr(request.user, 'developer') and matching_game.developer == request.user.developer
        if owned:
            user_rating = OwnedGame.objects.get(user_id=request.user, game_id=matching_game).rating
            play_time = OwnedGame.objects.get(user_id=request.user, game_id=matching_game).time_played
            if play_time > 60:
                hours = play_time // 60
                minutes = play_time - hours * 60
            else:
                hours = 0
                minutes = play_time
    else:
        in_cart = False
        owned = False
        developed = False

    context = {
        'game': matching_game,
        'discounted_price': discounted_price,
        'avg_rating': avg_rating['rating__avg'],
        'rates': rates,
        'user_rating': user_rating,
        'top_players': top_players,
        'similar_games': similar_games,
        'in_cart': in_cart,
        'owned': owned,
        'hours': hours,
        'minutes': minutes,
        'developed': developed
    }
    return render(request, 'gamebrowser/game_view.html', context)

@login_required
def play_game(request, game_id):

    # Check if the user owns the game that they are willing to play
    game_to_play = get_object_or_404(Game, id=game_id)
    owned_games = OwnedGame.objects.filter(user_id=request.user)

    # Get Game objects through OwnedGame objects
    users_game_objects = []
    for owned_game in owned_games:
        game_object = get_object_or_404(Game, id=owned_game.game_id_id)
        users_game_objects.append(game_object)

    if game_to_play not in users_game_objects:
        # If the user doesn't own the game, they are redirected to the game's info page
        # from which they can buy the game instead of trying to access it through url :)
        return redirect(reverse('game-view', args=[game_id]))

    # Get game origin for message checking in template
    # For example http://webcourse.cs.hut.fi/example_game.html
    # has origin of http://webcourse.cs.hut.fi/ which is returned
    # also from javascript event.origin in incoming message
    origin = game_to_play.url[:10]
    char_iterator = 10

    # Ensure that there's a trailing slash in game origin address after protocol
    if "/" not in game_to_play.url[char_iterator:]:
        game_to_play.url += "/"

    # Create origin
    while game_to_play.url[char_iterator] != "/":
        origin += game_to_play.url[char_iterator]
        char_iterator += 1

    context = {'game': game_to_play, 'origin': origin}

    return render(request, 'gamebrowser/play_game.html', context)

@login_required
def update(request, game_id):

    # Return empty response if the update request isn't AJAX
    # which means that it's not called by gameplay javascript
    if not request.is_ajax():
        return HttpResponse(status=204)

    # Update Game objects with data provided by messages from the game
    # Get the user's game entry in OwnedGame through request parameter of game_id
    game = get_object_or_404(Game, id=game_id)
    owned_game = get_object_or_404(OwnedGame, game_id=game, user_id=request.user)
    messagetype = request.GET.get('messageType')

    # Choices for different message types from the game to the service
    if messagetype == 'SCORE':
        # Check if score exceeds highscore and update if true
        if float(request.GET.get('score')) > owned_game.score:
            owned_game.score = float(request.GET.get('score'))
            owned_game.save()
            return JsonResponse({'highscore':'yes'})
        else:
            return JsonResponse({'highscore':'no'})

    elif messagetype == 'SAVE':
        # Save provided gamestate and return HTTP 200 indicating success
        owned_game.state = request.GET.get('gameState', None)
        owned_game.save()
        return HttpResponse(status=200)

    elif messagetype == 'LOAD_REQUEST':
        # Fetch gamestate and return it
        gamestate = owned_game.state
        return JsonResponse({'data':gamestate})

@login_required
def add_time(request, game_id):

    # Check if request is ajax to prevent adding time by url
    if not request.is_ajax():
        return HttpResponse(status=204)

    # Add one minute to playing time and return successful response
    # This view is called every minute by gameplay.js function timeUpdater
    game = get_object_or_404(Game, id=game_id)
    owned_game = get_object_or_404(OwnedGame, game_id=game, user_id=request.user)
    owned_game.time_played += 1
    owned_game.save()
    return HttpResponse(status=200)

@login_required
def submit_rating(request, game_id):

    # Check if rating is valid, i.e. something in 1-5 whole stars
    if int(request.GET.get('rating')) not in [1,2,3,4,5]:
        return HttpResponse(status=204)

    # Check if request is ajax to prevent adding ratings by url
    if not request.is_ajax():
        return HttpResponse(status=204)

    # Set the rating to database
    game = get_object_or_404(Game, id=game_id)
    owned_game = get_object_or_404(OwnedGame, game_id=game, user_id=request.user)
    owned_game.rating = request.GET.get('rating')
    owned_game.save()
    avg_rating = game.owners.aggregate(Avg('rating'))
    rates = OwnedGame.objects.filter(rating__gt=0, game_id=game).count()
    return JsonResponse({'data':avg_rating['rating__avg'], 'count':rates})


@login_required
def change_cart(request):
    #A helper view for modifying the shopping cart. The user is not supposed
    #to see this view. It returns the number of items in cart.
    action = request.GET.get("action")
    if action == "empty":
        ShoppingCart.objects.filter(user_id=request.user).delete()
        return HttpResponse(0)

    game_id = request.GET.get("game_id")
    matching_game = get_object_or_404(Game, id=game_id)

    found = ShoppingCart.objects.filter(user_id=request.user, game_id=matching_game)
    if found.exists(): #If found in shopping shopping cart, remove it.
        found.all().delete() #all() just in case there are duplicates (should not be)
    else: #Add to shopping cart, if the game is not developed or owned by the user.
        if not OwnedGame.objects.filter(user_id=request.user, game_id=matching_game).exists() and not (hasattr(request.user, 'developer') and matching_game.developer == request.user.developer):
            entry = ShoppingCart(user_id=request.user, game_id=matching_game)
            entry.save()


    return HttpResponse(len(ShoppingCart.objects.filter(user_id=request.user)))
