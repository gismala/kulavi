/*This script handles the AJAX page loading in the game browser*/
$.ajaxSetup({cache: false});

/*Generic game list reloading function*/
/*newState: true: Create a new entry to browser history*/
/*          false: Update existing entry (when staying on the same page)*/
/*clearList: true: Clear the game list before showing the new one*/
/*           false: Keep the game list visible (e.g. when adding to cart)*/
/*newUrl: If provided (page number clicked), load this url. If not, build new*/
/*        url from the search form.*/
function reloadResults(newState, clearList, newUrl) {
    if(!newUrl) {
      var check = "asc";
      if($("#search-asc-desc-toggle").prop("checked")) {check = "desc";}
      newUrl = "?query_tag="+$("#search-tag").val()+"&query_text="+$("#search-box").val().replace(/ /g,"+")+"&order_by="+$("#search-order").val()+"&order_dir="+check+"&page_number=1&per_page="+$("#search-per-page").val();
    }
    var state = {query: newUrl};
    if(clearList) { $("#search-results").empty(); }
    $("#search-results").load(newUrl, complete = function() {
      if(newState) {
        window.history.pushState(state,"", newUrl);
      }
      else {
        window.history.replaceState(state,"", newUrl);
      }
    });
}

/*Search button clicked*/
$("#search-button").on("click", function(event) {
    event.preventDefault();
    reloadResults(true, true);
});

/*Page changed*/
$(document).on("click", "#search-result-pages a", function(event) {
    event.preventDefault();
    reloadResults(true, true, $(this).attr("href"));
});

/*Initial history state when arriving on page*/
$(document).ready(function(event) {
    reloadResults(false, true, window.location.href);
});

/*User clicked back button*/
$(window).on('popstate', function(event) {
    var newUrl = event.originalEvent.state.query;
    var pageNum = event.originalEvent.state.page;
    $("#search-page-hidden").val(pageNum);
    $("#search-results").empty();
    $("#search-results").load(newUrl);
});

/*Ordering option changed, select default asc/desc*/
$("#search-order").on("change", function() {
    var check = false;
    if($("#search-order").val()==="name") {check = false;}
    if($("#search-order").val()==="price") {check = false;}
    if($("#search-order").val()==="release_date") {check = true;}
    if($("#search-order").val()==="update_date") {check = true;}
    $("#search-asc-desc-toggle").prop("checked", check);
});
