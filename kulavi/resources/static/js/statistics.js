﻿//Original window widht and height
var winWidth = $(window).width();
var winHeight = $(window).height();
var iframe = null;

//Start the AJAX request to load a map
load_map = function () {
    if ($("#map_not_loaded").length) {
        $("#statistics_map").load("map");
    }
}

//Resize the iframe according to the screen size and map size
resizeIframe = function (obj) {
    iframe = obj;
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';

    $(window).resize(function () {
        //Call resizeIframe method only if the window size has actually changed
        var onResize = function () {
            resizeIframe(iframe);
        }

        var winNewWidth = $(window).width(),
        winNewHeight = $(window).height();

        if (winWidth != winNewWidth || winHeight != winNewHeight) {
            resizeTimeout = window.setTimeout(onResize, 10);
        }
        winWidth = winNewWidth;
        winHeight = winNewHeight;
    });
}


$('th').click(function () {
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc) { rows = rows.reverse() }
    for (var i = 0; i < rows.length; i++) { table.append(rows[i]) }
})
function comparer(index) {
    return function (a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
    }
}
function getCellValue(row, index) {
    var td = $(row).children('td').eq(index);
    return $("span", td).html();
}



$(document).ready(function () {
    load_map();
});
