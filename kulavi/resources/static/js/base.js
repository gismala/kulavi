$(document).on("click", ".checkout-button", function (event) {
    //Check if there are items in the cart before letting the user go to payment page
    event.preventDefault();
    if($("#total-hidden").val()==="0") {
      $("#error-container").html("<div class=\"alert alert-danger\" role=\"alert\">Your shopping cart is empty!</div>");
    }
    else {
      window.location = $(this).parent().attr("href");
    }
});

$(document).on("click", ".empty-cart-button, .add-to-cart-button:not(.no-ajax)", function (event) {
    event.preventDefault();
    if(!$(this).hasClass("disabled")) {
      $(this).addClass("disabled");
      var thisHolder = $(this);
      var newUrl = $(this).parent().attr("href");
      $.ajax(newUrl).done(function(data) {
        /*Increase or decrease cart counter in navbar*/
        $("#cart-counter").html(data);
        if(thisHolder.hasClass("game-view")) {
          /*Viewing a single game, change button text*/
          thisHolder.toggleClass("in-cart");
        }
        else {
          /*Viewing game list, reload list*/
          reloadResults(false, false, window.location.href);
        }
        thisHolder.removeClass("disabled");
      });
    }
});

$(document).ready(function () {
    console.log("The base HTML loaded");
});
