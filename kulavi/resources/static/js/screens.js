var currentScreen = 0;

$('.prev-shot').on('click', function(e) {
  e.preventDefault();
  updateScreens(-1);
});

$('.next-shot').on('click', function(e) {
  e.preventDefault();
  updateScreens(1);
});

$(document).ready(function(){
  updateScreens(0);
});

function updateScreens(i) {
  var screens = $('.screenshot');
  var l = screens.length;
  currentScreen = (((currentScreen + i) % l) + l) % l
  screens.hide();
  $(screens[currentScreen]).show();
}
