/* Rating submit handlers */

// Change user's personal rating stars according to submitted rating
function changeStars(amount){
  // First make all stars empty if the user lowers their rating
  var j = 1;
  while (j <= 5){
    var parsedId = '#rate-'+j.toString()+'-icon';
    $(parsedId).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
    j++;
  }
  // Make stars below given rating filled
  var i = 1;
  while (i <= amount){
    var parsedId_low = '#rate-'+i.toString()+'-icon';
    $(parsedId_low).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
    i++;
  }
  return;
}

// Change average rating stars when game page is loaded and after submitting ratings
function avgStars(amount){
  amount = parseInt(amount);
  var m = 1;
  while (m <= 5){
    var parsedId_avg_empty = '#avgrate-'+m.toString()+'-icon';
    $(parsedId_avg_empty).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
    m++;
  }
  var n = 1;
  while (n <= amount){
    var parsedId_avg = '#avgrate-'+n.toString()+'-icon';
    $(parsedId_avg).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
    n++;
  }
  return;
}

// Submit a new rating to database with ajax call
function submitRating(rating){
  var url_original = window.location.toString();
  var url = url_original.replace('view', 'rate');
  var data = {'rating': rating};
  // Update the database and average rating after new rating
  $.ajax({type: 'GET', url: url, data: data,
          success: function (response) {
              avgStars(Math.round(response.data));
              $('#avgrating').replaceWith('<p>Average rating from '+response.count+' ratings</p>');
          }
          });
  return;
}

// Handlers for each rating star separately
$('#rate-1').on('click', function(e) {
  submitRating(1);
  changeStars(1);
});
$('#rate-2').on('click', function(e) {
  submitRating(2);
  changeStars(2);
});
$('#rate-3').on('click', function(e) {
  submitRating(3);
  changeStars(3);
});
$('#rate-4').on('click', function(e) {
  submitRating(4);
  changeStars(4);
});
$('#rate-5').on('click', function(e) {
  submitRating(5);
  changeStars(5);
});
