﻿var mediaurl = $("#media_url").html();
var defaultpic = mediaurl+'defaultpics/default_profilepic.png';
var file_upload_text = 'Upload a profile picture';
var screenshot_upload_text = 'Upload a screenshot';

set_styles = function () {
    //Set some styles for the forms that are implemented automatically
    $("form:not(.form-nondefault) :input").addClass("form-control");
    $("form input[type=email]").prop('required', true);
    $("form .errorlist").addClass("alert-danger").addClass("alert");
    $("#formset p label").html("Screenshot");

    //Remove django's preset imagefield elements when updating form that has images
    var a = $("form:not(.form-nondefault) p a");
    if (a.length > 0) {
      a[0].previousSibling.nodeValue = null;
      a[0].nextSibling.nextSibling.nextSibling.nodeValue = null;
      a[0].remove();
    }
};

hide_null_screenshots = function (initial) {
    //Hide formset forms that doesn't have a screenshot so that only one can be uploaded at a time
    var len = $("#formset p").length
    if (len) {
        var hadImage = 0;
        var hasImage = 0;
        var nOfImages = 0;
        $("div#formset p").each(function (i) {
            hasImage = $(".image_input_thumbnail", this).length;
            if (!hadImage && !hasImage && !initial) {
                if (!$(this).hasClass("hidden")) $(this).addClass("hidden");
            } else {
                $(this).removeClass("hidden");
                nOfImages++;
            }
            hadImage = hasImage;
            initial = false;
            if (i === len-1 && !nOfImages ) $(this).removeClass("hidden");
        });
    }
}

change_behavior = function () {
    hide_null_screenshots(true);
    if ($("#gameform").length)
    {
        defaultpic = mediaurl+'defaultpics/default_thumbnail.png';
        file_upload_text = 'Upload a game thumbnail';
    }

    $("form p").each(function () {
        if ($("input", this) && $("span", this)) {
            $("input", this).attr("placeholder", $("span", this).text());
            var field = $("input[type=file]", this);

            if (field.attr("type") == "file") {
                var isFormset = field.closest('#formset').length;
                var label = $("label", this);
                //Extend the file input label to have the custom "button" for uploading the picture
                var buttontext = isFormset ? screenshot_upload_text : file_upload_text;
                $("label", this).append("<br><span class='image_input_span btn btn-info'>" + buttontext + "</span><br>");

                //Use "thumbnail" of the image
                if (!$("#uploaded_image_thumbnail").length) {
                    //Create thumbnail if the field is not a formset
                    if (!isFormset)
                    {
                        $("label", this).append("<img class='image_input_thumbnail' src='" + defaultpic + "' height='128' alt='Profilepic'>");
                        $("label", this).append("<br><span class='defaut_image_span btn btn-default disabled'>Default picture</span>");
                    }

                }
                else if (!isFormset)
                {
                    $("label", this).append("<img class='image_input_thumbnail' src='" + $("#uploaded_image_thumbnail")[0].src + "' height='128' alt='Profilepic'>");
                }
                else if ($(this).nextUntil("p", ".formset_image").length)
                {
                    label.append("<img class='image_input_thumbnail' src='" + $(this).nextUntil("p", ".formset_image")[0].src + "' height='128' alt='Profilepic'>");
                    hide_null_screenshots(false);
                }

                //Occurs when the image is being uploaded
                field.on("change", function (e) {
                    //Set the file name to the button text
                    $(".image_input_span", label).html(e.target.value.split('\\').pop());

                    //Allow the default button to work
                    var defpic = $(".defaut_image_span", label);
                    if (defpic.length && defpic.hasClass("disabled")) {
                        defpic.removeClass("disabled");
                        defpic.on("click", function (e) {
                            e.preventDefault();
                            //Reset the form and buttons
                            field.val('');
                            $(".image_input_thumbnail", label)[0].src = defaultpic;
                            $(".image_input_span", label).html(file_upload_text);
                            defpic.addClass("disabled");
                        });
                    }

                    //Load the preview of the image
                    var file = field[0].files[0];
                    console.log(file.type);
                    if (file.size < 5242880 && file.type.match("image.*")) {
                        var reader = new FileReader();
                        reader.addEventListener("load", function () {
                            //For formset add the preview images
                            if (!$(".image_input_thumbnail", label).length) {
                                label.append("<img class='image_input_thumbnail' src='" + reader.result + "' height='128' alt='Profilepic'>");
                                label.append("<span class='empty_screenshot btn btn-default btn-xs'>Delete Screenshot</span>");
                                $(".image_limit", label).remove();
                                hide_null_screenshots(false);
                            }
                            $(".image_input_thumbnail", label)[0].src = reader.result;

                            //Delete screenshot if not needed
                            $(".empty_screenshot", label).on("click", function (e) {
                                e.preventDefault();
                                field.val('');
                                $(".image_input_span", label).html(buttontext);
                                $(".image_input_thumbnail", label).remove();
                                this.remove();
                                hide_null_screenshots(true);
                            });
                        }, false);

                        if (file) {
                            reader.readAsDataURL(file);
                        }
                    } else {
                        //The file exceeds the size limit or the file is not an image
                        var message = file.type.match("image.*") ? "The image size exceeds the 5Mt limit" : "The file is not in supported image format";
                        field.val('');
                        $(".image_input_span", label).html(buttontext);
                        $(".image_input_thumbnail", label).remove();
                        $(".empty_screenshot", label).remove();
                        label.append("<p class='image_limit alert alert-danger'>"+message+"</p>");

                    }
                });
            };
        }
    });
};

$(document).ready(function () {
    set_styles();
    change_behavior();
});
