// Idle time variable
var idletime = 0;

$(document).ready(function(){

  // Update game playing time every minute (60000 ms)
  var idleinterval = setInterval(timeUpdater, 60000);
  // Events that reset idle time counter
  // Actual actions instead of just moving the mouse
  $(this).click(function(e){idletime=0;});
  $(this).contextmenu(function(e){idletime=0;});
  $(this).dblclick(function(e){idletime=0;});
  $(this).keypress(function(e){idletime=0;});
  $(this).mousedown(function(e){idletime=0;}); // Drag (touchscreen)

});

function timeUpdater(){
  idletime++;
  var url_original = window.location.toString();
  var url = url_original.replace('playgame', 'addtime');
  // Stop adding play time after user has been 5 minutes idle
  if (idletime <= 5) { $.get(url); }
}
