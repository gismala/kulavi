import html5lib
from contextlib import closing
from urllib.request import urlopen

parser = html5lib.HTMLParser(strict=True)

with closing(urlopen("http://kulavistore.herokuapp.com/browse/home/")) as f:

    doc = parser.parse(f, transport_encoding=f.info().get_content_charset())
    for e in parser.errors:
        print(e)
