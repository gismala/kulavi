"""kulavi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import os
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView

LOGIN_APP_URL = 'profile'
DEVSTATISTISC_APP_URL = 'statistics'

urlpatterns = [
    url(r'^$', RedirectView.as_view(pattern_name='home', permanent=True)),
    url(r'^admin/', admin.site.urls),
    url(r'^{0}/'.format(LOGIN_APP_URL), include('login.urls')),
    url(r'^browse/', include('gamebrowser.urls')),
    url(r'^payments/', include('payments.urls')),
    url(r'^api/v1/', include('restapi.urls')),
    url(r'^{0}/'.format(DEVSTATISTISC_APP_URL), include('devstatistics.urls')),
]

if "DYNO" not in os.environ:
    urlpatterns+=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    #urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
