from django.apps import AppConfig


class DevstatisticsConfig(AppConfig):
    name = 'devstatistics'
