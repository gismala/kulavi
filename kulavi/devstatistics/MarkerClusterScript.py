from folium.plugins import MarkerCluster

#Modified from https://github.com/python-visualization/folium/issues/416
class MarkerClusterScript(MarkerCluster):
    def __init__(self, data, callback):
        from jinja2 import Template
        super(MarkerClusterScript, self).__init__([])
        self._name = 'Script'
        self._data = data
        if callable(callback):
            from flexx.pyscript import py2js
            self._callback =  py2js(callback, new_name="callback")
        else:
            self._callback = "var callback = {};".format(_callback)

        self._template = Template(u"""
                            {% macro script(this, kwargs) %}
                            (function(){
                                var data = {{this._data}};
                                var map = {{this._parent.get_name()}};
                                var cluster = L.markerClusterGroup({maxClusterRadius: 50, zoomToBoundsOnClick: false, spiderfyDistanceMultiplier: 2});
                                {{this._callback}}
                                cluster.on('clusterclick', function (a) {
                                    a.layer.spiderfy();
                                });
                                for (var i = 0; i < data.length; i++) {
                                    var row = data[i];
                                    var marker = callback(row);
                                    marker.addTo(cluster);
                                }

                                cluster.addTo(map);
                            })();
                            {% endmacro %}
                                        """)