from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.display_statistics, name='dev-statistics'),
    url(r'^map$', views.display_statistics, name='statistics-map'),
    url(r'^pie.png$', views.pie_chart, name='statistics-pie'),
    url(r'^ages.png$', views.age_distribution, name='statistics-ages'),
    ]
