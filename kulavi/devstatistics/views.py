import random
import datetime
from collections import defaultdict
from random import choice
from login.models import COUNTRY_CODES
from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
from django.db.models import Count, Sum, Avg
from django.contrib.auth.decorators import login_required
from gamebrowser.models import Game, OwnedGame
from payments.models import PaymentHistory

##Imports for charts
import matplotlib
matplotlib.use('Agg') #Fix ktinker error in Heroku
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
import matplotlib.style as style
import matplotlib.cm as cm
import pandas as pd
import numpy as np

#Imports for a map
from folium import Map
from .MarkerClusterScript import MarkerClusterScript



PLOTSTYLE = 'ggplot'
MAPSTYLE = 'Cartodb Positron'
datasets = {}
games = None


###########Helper functions##############
def get_games(dev):
    global games
    if not games:
        all_games = Game.objects.filter(developer=dev.pk)
        games = all_games.annotate(payments_count=Count('payments')).values('id').order_by('-payments_count')[0:5]
        if len(games) < 5:
            return None
    return games

def set_colors_to_map(data):
    colors = []
    uniq_games = sorted(list(set(data)))
    available_colors = ['red', 'blue','purple','gray', 'orange', 'green','darkred', 'lightred', 'beige', 'darkblue', 'darkgreen', 'cadetblue', 'darkpurple', 'white', 'pink', 'lightblue', 'lightgreen', 'black', 'lightgray'][:len(uniq_games)]
    for i in range(len(data)):
        colors.append(available_colors[uniq_games.index(data[i])])
    return colors

def generate_map(games):
    """Generates lealet map using the great folium package"""
    df = pd.DataFrame(list(OwnedGame.objects.filter(game_id_id__in=games).exclude(user_id__profile__latitude__isnull=True).values_list("game_id__name", "user_id__profile__latitude", "user_id__profile__longitude")))
    df['lat'] = df[1].astype(float)
    df['lon'] = df[2].astype(float)
    color= set_colors_to_map(df[0].tolist())
    map_data = pd.DataFrame({'lat':df['lat'], 'lon':df['lon'], 'color':color, 'game':df[0]})

    #Create actual map, do not give any data there on server side
    map = Map(location=[20, 50], zoom_start=2, tiles=MAPSTYLE)

    #Python function, which will be translated to javascript using py2js
    #Generates individual marker for each point
    def create_marker(row):
        icon = L.AwesomeMarkers.icon({markerColor: row.color})
        marker = L.marker(L.LatLng(row.lat, row.lon))
        marker.setIcon(icon)
        marker.bindPopup(row['game'])
        return marker

    #Use external class to turn marker cluster creation in to the javascript rather than folium.
    #Marker cluster creation in pure python is too slow, so better to do it on the client side
    MarkerClusterScript(map_data.to_json(orient="records"), callback=create_marker).add_to(map)

    #Turn whole map to html in order to present it in a template
    html = map._repr_html_()
    html = html.replace('"', '&quot;')
    embed_html = '<iframe srcdoc="{0}" scrolling="no" style="width: 100%; order: none" onload="resizeIframe(this)"></iframe>'.format(html)
    return embed_html

def graph_returner(fig):
    fig.patch.set_facecolor('none')
    canvas=FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

###########Views##############
@login_required
def display_statistics(request):
    global games
    games = None
    #Main view ment for developers to inspect statistics on the developed games
    if not hasattr(request.user, 'developer'):
        return render(request, 'devstatistics/display_statistics.html')
    dev = request.user.developer
    if request.is_ajax():
        map_html = generate_map(get_games(dev))
        return render(request, 'devstatistics/statistics_map.html', {'map':map_html})
    games = get_games(dev)
    all_games = Game.objects.filter(developer=dev.pk).order_by('name')
    totals = PaymentHistory.objects.filter(game_id_id__in=all_games).order_by('payment_time').values(
        "game_id__name", "game_id", "payment_time", "amount")

    context = {
        'dev' : dev,
        'games' : games,
        'all_games' : all_games,
        'totals':totals,
        'total_earnings' : all_games.aggregate(Sum("payments__amount")),
        'total_boughts' : all_games.aggregate(Count("payments")),
        'total_playtime' : all_games.aggregate(Sum("owners__time_played")),
        'total_ratings' : all_games.aggregate(Avg('owners__rating')),
        }
    return render(request, 'devstatistics/display_statistics.html', context)

@login_required
def age_distribution(request):
    '''Graph of the age distribution per game'''
    style.use(PLOTSTYLE)
    fig = Figure()
    if not hasattr(request.user, 'developer'):
        return graph_returner(fig)
    games = get_games(request.user.developer)
    if games:
        #Create subplot, which will be plotted to canvas
        ax = fig.add_subplot(111)
        df = pd.DataFrame(list(OwnedGame.objects.filter(game_id_id__in=games).values_list("game_id__name", "user_id__profile__birthdate")))
        year = datetime.date.today().year
        df['age'] = [year - d.year for d in df[1]]

        #Reset dataframe to have only needed data
        df = pd.DataFrame({'agegroup' : pd.cut(df['age'], [10, 15, 25, 35, 45, 100],
                                labels=['10-15', '16-25', '26-35', '36-45', '>45']),
                           'game':df[0]})

        #Add extra column with one to be able to group by age and game
        df['extra'] = 1
        df = df.groupby(['agegroup', 'game']).sum().unstack()
        #Use game column
        df.columns = df.columns.droplevel()
        #Plot histogram
        df.plot.bar(ax=ax)
    return graph_returner(fig)

@login_required
def pie_chart(request):
    fig=Figure()
    ax=fig.add_subplot(111)
    if not hasattr(request.user, 'developer'):
        return graph_returner(fig)
    games = get_games(request.user.developer)
    if games:
        data = list(PaymentHistory.objects.filter(game_id_id__in=games).values_list("game_id__name").annotate(Count("id")))
        game_names = [row[0] for row in data]
        # Pie chart, where the slices will be ordered and plotted counter-clockwise:
        sizes = [row[1] for row in data]
        explode = tuple(map(lambda x: 0.1 if x==max(sizes) else 0, sizes))
        #explode = (0, 0.1, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

        ax.pie(sizes, explode=explode, labels=game_names,autopct=lambda val:np.round(val/100*sum(sizes), 0),
                shadow=True, startangle=90)
        ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    return graph_returner(fig)
