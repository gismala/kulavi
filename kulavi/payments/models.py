from django.db import models
from django.contrib.auth.models import User

class PaymentHistory(models.Model):
    # Objects in this table represent one user buying one game
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='payments')
    game_id = models.ForeignKey('gamebrowser.Game', on_delete=models.CASCADE, related_name='payments')
    payment_time = models.DateTimeField(blank=False)
    amount = models.DecimalField(max_digits=9, decimal_places=2, blank=False)

class BulkPayment(models.Model):
    # Objects in this table represent one payment event that may contain various games
    # This needs to be created in order to recieve payment ids for payment service
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, related_name='bulk_payments')
    amount = models.DecimalField(max_digits=9, decimal_places=2, blank=False)
