from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db.models import Q
from django.utils import timezone

from .models import PaymentHistory, BulkPayment
from gamebrowser.models import Game, ShoppingCart, OwnedGame

from hashlib import md5
import os

# Constants for payments service
SID = "kulavi"
SECRET_KEY = "dd5762537c465065ffe31c4f4640327a"

@login_required
def get_payments(request, date_start=None, date_end=None):

    # Get payments for user searching them
    # Return 404 if there's no payments
    user = request.user
    user_id = user.id
    matching_payments = PaymentHistory.objects.filter(user_id__id=user_id)

    if not matching_payments:
        return render(request, "payments/payments_list.html", {'empty': 'yes'})

    # Order by descending date, so newest payments first
    matching_payments = matching_payments.order_by('-payment_time')

    # Payment information to template
    payments_sum = 0
    payment_entries = []
    for payment in matching_payments:
        payments_sum += payment.amount
        entry = {}
        game = Game.objects.get(id=payment.game_id.id)
        entry['game'] = game.name
        entry['gameid'] = game.id
        entry['date'] = payment.payment_time
        entry['amount'] = payment.amount
        payment_entries.append(entry)

    context = {'payments': payment_entries, 'sum': payments_sum}
    return render(request, "payments/payments_list.html", context)


def get_shopping_cart(user):

    # Helper function
    # Returns the contents of an user's shopping cart
    games_in_cart = ShoppingCart.objects.filter(user_id=user)
    return games_in_cart


def get_discounted_price(game):

    # Helper function
    # Returns discounted price for a game
    return float(game.price) - ((float(game.discount) / 100) * float(game.price))


def get_users_games(user):

    # Helper function
    # Returns game objects that an user owns
    users_games = OwnedGame.objects.filter(user_id=user)
    users_games_list = []
    for game in users_games:
        users_games_list.append(Game.objects.get(id=game.game_id.id))
    return users_games_list


@login_required
def make_payment(request):

    # Get the contents of an user's shopping cart and already owned games
    games_in_cart = get_shopping_cart(request.user)
    if not games_in_cart:
        msg = 'There are no games in your shopping cart anymore.'
        return render(request, 'payments/payment_error.html', {'message':msg})
    users_games = get_users_games(request.user)

    # Calculate the sum of all game prices in shopping cart
    payment_sum = 0.0
    # All Game objects are appended to a list for iteration in template
    games_objects = []
    pidstring = ":"

    for game_in_cart in games_in_cart:

        game = Game.objects.get(id=game_in_cart.game_id.id)
        # Check if the user has somehow managed to add already owned games to shopping cart
        # If yes, the payment can't proceed because no one wants to pay twice for one game
        if game in users_games:
            msg = 'You already own one or more games in your shopping cart.\
                   Remove those games to proceed with the payment.'
            return render(request, 'payments/payment_error.html', {'message':msg})
        # Calculate sum of game prices in shopping cart
        if not game.discount:
            payment_sum += float(game.price)
        else:
            game.discounted_price = get_discounted_price(game)
            payment_sum += game.discounted_price
        pidstring += str(game.id) + '/'
        games_objects.append(game)

    payment_sum = round(payment_sum, 2) # Payment service requires 2 decimals max

    # Create an BulkPayment object in order to recieve an id for the payment
    # This is created because PaymentHistory tracks gamewise payments
    # and there could be various games bought in one payment. Another use
    # for BulkPayment entry is to check that the payment is valid.
    payment_bulk = BulkPayment(user_id=request.user, amount=payment_sum)
    payment_bulk.save()

    # Create checksum for payment service
    pid = str(payment_bulk.id) + ':' + str(payment_bulk.amount) + pidstring
    checksumstr = "pid={}&sid={}&amount={}&token={}".format(pid, SID, payment_sum, SECRET_KEY)
    c = md5(checksumstr.encode("ascii"))
    checksum = c.hexdigest()

    # Urls to be called from Simple Payments depending on if deployed in Heroku or not
    if "DYNO" in os.environ:
        success = "https://kulavistore.herokuapp.com/payments/payment_success"
        cancel = "https://kulavistore.herokuapp.com/payments/payment_cancel"
        error = "https://kulavistore.herokuapp.com/payments/payment_error"
    else:
        success = "http://localhost:8000/payments/payment_success"
        cancel = "http://localhost:8000/payments/payment_cancel"
        error = "http://localhost:8000/payments/payment_error"

    context = { 'games': games_objects,
                'pid': pid,
                'sid': SID,
                'amount': payment_sum,
                'checksum': checksum,
                'success': success,
                'cancel': cancel,
                'error': error
                }
    return render(request, 'payments/make_payment.html', context)

@login_required
def payment_success(request):

    pid = request.GET.get('pid')
    ref = request.GET.get('ref')
    result = request.GET.get('result')
    checksum = request.GET.get('checksum')

    # Check that incoming request has a success result
    if result != 'success':
        msg = 'The message coming from Simple Payment says that this payment \
                was not successful'
        return render(request, 'payments/payment_error.html', {'message': msg})

    # Check that there is a trace of payment specified with pid in request
    # and it is not a copy of some previous payment
    try:
        BulkPayment.objects.get(Q(id=pid.split(':')[0]), Q(amount=float(pid.split(':')[1])))
    except ObjectDoesNotExist:
        msg = 'The pid of this payment is not valid.'
        return render(request, 'payments/payment_error.html', {'message': msg})
    except MultipleObjectsReturned:
        msg = 'The pid has already been used: your payment is not valid.'
        return render(request, 'payments/payment_error.html', {'message': msg})

    # Calculate checksum from parameters coming from Simple Payments
    valid_string = "pid={}&ref={}&result={}&token={}".format(pid, ref, result, SECRET_KEY)
    c = md5(valid_string.encode("ascii"))
    valid_checksum = c.hexdigest()

    # If the checksum calculated from success url parameters differs from
    # checksum that is calculated in the payment service, the user has cheated
    if checksum != valid_checksum:
        msg = 'You tried modify the payment result through confirmation url. \
               No success.'
        return render(request, 'payments/payment_error.html', {'message': msg})

    # Prepare for creating history entries
    user = request.user
    games_in_cart = get_shopping_cart(user)
    users_games = get_users_games(user)
    payment_time = timezone.now()
    already_owned = 0

    # Calculate the total price of games in cart at the time of payment
    # response coming from the payments service
    cartsum = 0
    for game_in_buying in games_in_cart:
        game = Game.objects.get(id=game_in_buying.game_id.id)
        if game.discount > 0:
            cartsum += get_discounted_price(game)
        else:
            cartsum += float(game.price)

    # If the cart has been filled/emptied when the user has been in the payments
    # service, the user receives only games that were in the cart when moving
    # to payments service
    if float(cartsum) != float(pid.split(':')[1]):

        original_games_pid = pid.split(':')[2]
        original_games = original_games_pid.split('/')

        # Create entries for games determined by pid
        for gameid in original_games[0:len(original_games) - 1]:

            game = Game.objects.get(id=int(gameid))
            # Add entry to PaymentHistory for each game bought
            if game.discount > 0:
                paid_amount = get_discounted_price(game)
            else:
                paid_amount = float(game.price)
            payment_entry = PaymentHistory(user_id=user, game_id=game, payment_time=payment_time, amount=paid_amount)
            payment_entry.save()

            # Add entry to user's owned games
            owned_game_entry = OwnedGame(user_id=user, game_id=game, time_played=0,rating=0)
            owned_game_entry.save()

            # Delete original games from shopping cart in the case that the shopping
            # cart was filled while in payments service; otherwise there's nothing to empty
            try:
                game_in_cart = ShoppingCart.objects.get(Q(user_id=user), Q(game_id=game))
                game_in_cart.delete()
            except ObjectDoesNotExist:
                pass

        msg = 'Your shopping cart contents changed while in payment service.\
                Only games that were originally in the cart were bought.'
        return render(request, 'payments/payment_success.html', {'message': msg})

    # Successful and non-modified payment changes database
    # Add entries to PaymentHistory and OwnedGame
    for game_in_cart in games_in_cart:

        game = Game.objects.get(id=game_in_cart.game_id.id)

        # Add entries only if the user doesn't own the game already to prevent duplicates
        # Duplicate games bought *shouldn't* be even possible
        if game not in users_games:
            # Add entry to PaymentHistory for each game bought
            if game.discount > 0:
                paid_amount = get_discounted_price(game)
            else:
                paid_amount = float(game.price)
            payment_entry = PaymentHistory(user_id=user, game_id=game, payment_time=payment_time, amount=paid_amount)
            payment_entry.save()

            # Add entry to user's owned games
            owned_game_entry = OwnedGame(user_id=user, game_id=game, time_played=0,rating=0)
            owned_game_entry.save()
        else:
            already_owned += 1

    # Delete games from shopping cart
    games_in_cart.delete()

    # Message displayed when user has somehow succeeded to buy a game twice
    # This, again, *shouldn't* be even possible because also the shopping cart
    # is checked before payment
    if already_owned != 0:
        msg = 'You tried to buy a copy of a game or games that you already own.\
               No duplicate games were created to your collection.'
    else:
        msg = False

    return render(request, 'payments/payment_success.html', {'message': msg})

@login_required
def payment_cancel(request):

    # View that is called when payment is canceled by the user
    # No changes to database made here
    return render(request, 'payments/payment_cancel.html', {})

@login_required
def payment_error(request):

    # View that is called when payment has been interrupted by an error
    # No changes to database made here
    # Error messages may come from elsewhere
    return render(request, 'payments/payment_error.html', {})
