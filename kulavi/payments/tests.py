from django.test import TestCase
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from gamebrowser.models import Game, Tag, Screenshot, OwnedGame, ShoppingCart
from .models import PaymentHistory, BulkPayment
from login.models import User, Developer
from login.forms import GameForm, ScreenshotForm, TagForm

from django.utils import timezone

# Create your tests here.

class PaymentsTestCase(TestCase):

    def setUp(self):

        self.valid_tag_names = ['RPG', 'Action', 'Puzzle']
        self.valid_tags = []

        for tag in self.valid_tag_names:
            t = Tag(name=tag)
            t.full_clean()
            t.save()
            self.valid_tags.append(t)

        # Create dummy developer
        self.devuser = User.objects.create_user(username = 'dev', password = 'salasana')
        self.dev = Developer(user = self.devuser, developer_name = 'dev', company = 'com', bank_account = '123')
        self.dev.save()

        # Create dummy user
        self.user = User.objects.create_user(username = 'user', password = 'salasana')
        self.user.save()

        # Create valid game fields
        self.valid_game_fields = {'name': 'Valid Game: Director\'s Cut',
            'url': 'http://www.validsoft.com/valid_game/play.php',
            'price': 49.99,
            'discount': 50,
            'short_description': 'Wowie-zowie! What a valid game!',
            'long_description': 'Perhaps the most valid game you\'ll ever see, Valid Game sets out to push the industry\'s standards on how valid a game can be.',
            'thumbnail': 'defaultpics/default_thumbnail.png',
            'tags': self.valid_tags
            }

    def test_payment_models(self):

        valid_game_form = GameForm(data = self.valid_game_fields)
        self.assertTrue(valid_game_form.is_valid(), 'Valid game form wasn\'t valid')

        # Create valid game
        valid_game = Game(name = valid_game_form.cleaned_data['name'],
            url = valid_game_form.cleaned_data['url'],
            price = valid_game_form.cleaned_data['price'],
            discount = valid_game_form.cleaned_data['discount'],
            short_description = valid_game_form.cleaned_data['short_description'],
            long_description = valid_game_form.cleaned_data['long_description'],
            thumbnail = valid_game_form.cleaned_data['thumbnail'],
            developer = self.dev)
        valid_game.save()
        valid_game.tags = valid_game_form.cleaned_data['tags']
        valid_game.save()

        valid_payment_fields = {'user_id': self.user,
                                'game_id': valid_game,
                                'payment_time': timezone.now(),
                                'amount': 10.00}

        # Create valid PaymentHistory entry
        valid_paymenthistory_entry = PaymentHistory(user_id=valid_payment_fields['user_id'],
                                    game_id=valid_payment_fields['game_id'],
                                    payment_time=valid_payment_fields['payment_time'],
                                    amount=valid_payment_fields['amount'])
        valid_paymenthistory_entry.save()

        self.assertEqual(getattr(valid_paymenthistory_entry, 'amount'), valid_payment_fields['amount'], 'Amount not saved correctly')
        self.assertEqual(getattr(valid_paymenthistory_entry, 'user_id'), valid_payment_fields['user_id'], 'Game buying user not saved correctly')
        self.assertEqual(getattr(valid_paymenthistory_entry, 'game_id'), valid_payment_fields['game_id'], 'Bought game not saved correctly')
        self.assertEqual(getattr(valid_paymenthistory_entry, 'payment_time'), valid_payment_fields['payment_time'], 'Payment time not saved correctly')

        another_entry = PaymentHistory(user_id=valid_payment_fields['user_id'],
                                    game_id=valid_payment_fields['game_id'],
                                    payment_time=timezone.now(),
                                    amount=1634656)
        another_entry.save()

        self.assertNotEqual(another_entry.payment_time, valid_paymenthistory_entry.payment_time, 'Payment times should differ withink two entries')
        self.assertNotEqual(another_entry.amount, valid_paymenthistory_entry.amount, 'Paid amounts should differ within two entries')

        valid_bulkpayment_fields = {'user_id': self.user,
                                    'amount': 20.00}

        # Create valid BulkPayment entry
        valid_bulkpayment_entry = BulkPayment(user_id=valid_bulkpayment_fields['user_id'],
                                                amount=valid_bulkpayment_fields['amount'])
        valid_bulkpayment_entry.save()

        self.assertEqual(getattr(valid_bulkpayment_entry, 'amount'), valid_bulkpayment_fields['amount'], 'Amount not saved correctly')
        self.assertEqual(getattr(valid_bulkpayment_entry, 'user_id'), valid_bulkpayment_fields['user_id'], 'Game buying user not saved correctly')
