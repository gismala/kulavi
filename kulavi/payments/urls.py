from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^payment_success/$', views.payment_success, name='payment-success'),
    url(r'^payment_cancel/$', views.payment_cancel, name='payment-cancel'),
    url(r'^payment_error/$', views.payment_error, name='payment-error'),
    url(r'^make_payment/$', views.make_payment, name='payment-make'),
    url(r'^search/$', views.get_payments, name='payment-search'),
]
