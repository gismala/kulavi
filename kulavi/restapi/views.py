from django.http import HttpResponse, Http404
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.db.models import Avg, Count
import json
import math

from gamebrowser.models import Game, Tag, OwnedGame
from login.models import Developer

DEFAULT_LIMIT = 10

#Views for the REST API. Supported resources and reference expansions:
#
#games
#games/<id>                     developer, highScores, similarGames
#games/<id>/similarGames
#games/<id>/highScores
#developers
#developers/<name>              games
#developers/<name>/games
#
#limit and offset are also supported.

#Helper function for getting offset & offset+limit query parameters
def get_offset_limit(request):
    offset = 0
    limit = DEFAULT_LIMIT
    if request.GET.get('offset'):
        try:
            offset = int(request.GET.get('offset'))
        except ValueError:
            offset = 0
    if request.GET.get('limit'):
        try:
            limit = int(request.GET.get('limit'))
        except ValueError:
            limit = DEFAULT_LIMIT
    return offset, limit

#Helper function for adding necessary attributes to collection resources
def add_collection(request, view, items, extraargs=None):
    offset, limit = get_offset_limit(request)

    uri = request.build_absolute_uri(reverse(view, args=extraargs))

    d = {}
    d['href'] = uri
    d['offset'] = offset
    d['limit'] = limit
    if len(items) != 0:
        d['first'] = {'href': uri+'?offset=0&limit='+str(limit)}
    else:
        d['first'] = None
    if offset == 0:
        d['previous'] = None
    else:
        prev_offset = max(offset-limit, 0)
        d['previous'] = {'href': uri+'?offset='+str(prev_offset)+'&limit='+str(limit)}
    if offset + limit >= len(items):
        d['next'] = None
    else:
        d['next'] = {'href': uri+'?offset='+str(offset+limit)+'&limit='+str(limit)}
    if len(items) != 0:
        d['last'] = {'href': uri+'?offset='+str(limit*math.ceil(len(items)/limit)-limit)+'&limit='+str(limit)}
    else:
        d['last'] = None

    return d

def all_games_rest(request, raw=False):
    offset, limit = get_offset_limit(request)

    items = Game.objects.all()
    g_dict = add_collection(request, 'all-games-rest', items)
    g_dict['items'] = [{'href': request.build_absolute_uri(reverse('single-game-rest', args=[g.pk]))} for g in items[offset:offset+limit]]

    if raw:
        return g_dict
    else:
        g_json = json.dumps(g_dict)
        if request.GET.get("callback"):
            g_json = request.GET.get("callback")+"("+g_json+")"
        return HttpResponse(g_json, content_type="application/json")

def single_game_rest(request, game_id, raw=False):
    expands = []
    if request.GET.get('expand'):
        expands = request.GET.get('expand').split(' ')

    try:
        game = Game.objects.get(pk=game_id)

        g_dict = {}
        g_dict['href'] = request.build_absolute_uri(reverse('single-game-rest', args=[game_id]))
        g_dict['id'] = game.pk
        g_dict['name'] = game.name
        g_dict['url'] = game.url
        g_dict['price'] = float(game.price)
        g_dict['discount'] = game.discount
        g_dict['developer'] = {'href': request.build_absolute_uri(reverse('single-developer-rest', args=[game.developer.developer_name]))} if game.developer != None else None
        if 'developer' in expands and g_dict['developer'] != None:
            g_dict['developer'] = single_developer_rest(request, game.developer.developer_name, raw=True)
        g_dict['shortDescription'] = game.short_description
        g_dict['longDescription'] = game.long_description
        g_dict['releaseDate'] = game.release_date.isoformat()
        g_dict['updateDate'] = game.update_date.isoformat()
        g_dict['tags'] = [t.name for t in game.tags.all()]
        g_dict['rating'] = game.owners.aggregate(Avg('rating'))['rating__avg']
        g_dict['highScores'] = {'href': request.build_absolute_uri(reverse('game-highscores-rest', args=[game_id]))}
        if 'highScores' in expands:
            g_dict['highScores'] = game_highscores_rest(request, game_id, raw=True)
        g_dict['similarGames'] = {'href': request.build_absolute_uri(reverse('similar-games-rest', args=[game_id]))}
        if 'similarGames' in expands:
            g_dict['similarGames'] = similar_games_rest(request, game_id, raw=True)

        if raw:
            return g_dict
        else:
            g_json = json.dumps(g_dict)
            if request.GET.get("callback"):
                g_json = request.GET.get("callback")+"("+g_json+")"
            return HttpResponse(g_json, content_type="application/json")
    except ObjectDoesNotExist:
        raise Http404("Game not found")

def similar_games_rest(request, game_id, raw=False):
    try:
        game = Game.objects.get(pk=game_id)

        offset, limit = get_offset_limit(request)

        items = Game.objects.filter(tags__in=game.tags.all()).annotate(Count('name')).exclude(pk=game.pk).order_by('-name__count')
        g_dict = add_collection(request, 'similar-games-rest', items, extraargs=[game.pk])
        g_dict['items'] = [{'href': request.build_absolute_uri(reverse('single-game-rest', args=[g.pk]))} for g in items[offset:offset+limit]]

        if raw:
            return g_dict
        else:
            g_json = json.dumps(g_dict)
            if request.GET.get("callback"):
                g_json = request.GET.get("callback")+"("+g_json+")"
            return HttpResponse(g_json, content_type="application/json")
    except ObjectDoesNotExist:
        raise Http404("Game not found")

def game_highscores_rest(request, game_id, raw=False):
    try:
        game = Game.objects.get(pk=game_id)

        offset, limit = get_offset_limit(request)

        items = game.owners.order_by('-score')
        g_dict = add_collection(request, 'game-highscores-rest', items, extraargs=[game.pk])
        g_dict['items'] = [{'player': p.user_id.username, 'score': p.score} for p in items[0:10]]

        if raw:
            return g_dict
        else:
            g_json = json.dumps(g_dict)
            if request.GET.get("callback"):
                g_json = request.GET.get("callback")+"("+g_json+")"
            return HttpResponse(g_json, content_type="application/json")
    except ObjectDoesNotExist:
        raise Http404("Game not found")

def all_developers_rest(request, raw=False):
    offset, limit = get_offset_limit(request)

    items = Developer.objects.all()
    d_dict = add_collection(request, 'all-developers-rest', items)
    d_dict['items'] = [{'href': request.build_absolute_uri(reverse('single-developer-rest', args=[d.developer_name]))} for d in items[offset:offset+limit]]

    if raw:
        return d_dict
    else:
        d_json = json.dumps(d_dict)
        if request.GET.get("callback"):
            d_json = request.GET.get("callback")+"("+d_json+")"
        return HttpResponse(d_json, content_type="application/json")

def single_developer_rest(request, dev_name, raw=False):
    expands = []
    if request.GET.get('expand'):
        expands = request.GET.get('expand').split(' ')

    try:
        dev = Developer.objects.get(developer_name=dev_name)

        d_dict = {}
        d_dict['href'] = request.build_absolute_uri(reverse('single-developer-rest', args=[dev_name]))
        d_dict['developerName'] = dev.developer_name
        d_dict['company'] = dev.company
        d_dict['games'] = {'href': request.build_absolute_uri(reverse('developer-games-rest', args=[dev_name]))}
        if 'games' in expands:
            d_dict['games'] = developer_games_rest(request, dev_name, raw=True)

        if raw:
            return d_dict
        else:
            d_json = json.dumps(d_dict)
            if request.GET.get("callback"):
                d_json = request.GET.get("callback")+"("+d_json+")"
            return HttpResponse(d_json, content_type="application/json")
    except ObjectDoesNotExist:
        raise Http404("Developer not found")

def developer_games_rest(request, dev_name, raw=False):
    try:
        dev = Developer.objects.get(developer_name=dev_name)

        offset, limit = get_offset_limit(request)

        items = dev.games.all()
        g_dict = add_collection(request, 'developer-games-rest', items, extraargs=[dev.developer_name])
        g_dict['items'] = [{'href': request.build_absolute_uri(reverse('single-game-rest', args=[g.pk]))} for g in items[offset:offset+limit]]

        if raw:
            return g_dict
        else:
            g_json = json.dumps(g_dict)
            if request.GET.get("callback"):
                g_json = request.GET.get("callback")+"("+g_json+")"
            return HttpResponse(g_json, content_type="application/json")
    except ObjectDoesNotExist:
        raise Http404("Developer not found")
