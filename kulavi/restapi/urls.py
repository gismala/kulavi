from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^games/$', views.all_games_rest, name='all-games-rest'),
    url(r'^games/([0-9]+)/$', views.single_game_rest, name='single-game-rest'),
    url(r'^games/([0-9]+)/similarGames/$', views.similar_games_rest, name='similar-games-rest'),
    url(r'^games/([0-9]+)/highScores/$', views.game_highscores_rest, name='game-highscores-rest'),
    url(r'^developers/$', views.all_developers_rest, name='all-developers-rest'),
    url(r'^developers/(\w+)/$', views.single_developer_rest, name='single-developer-rest'),
    url(r'^developers/(\w+)/games/$', views.developer_games_rest, name='developer-games-rest'),
]
