import os
import time
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kulavi.settings")
import django
django.setup()

from django.contrib.auth.models import User
from uuid import uuid4
from random import choice, randint, sample
from datetime import date, timedelta
import string


from gamebrowser.models import Game, Tag, OwnedGame
from login.models import Profile, Developer, COUNTRY_CODES
from payments.models import PaymentHistory

NAMES = [['Super Mario','Lara Croft','Sonic','Barbie','Master Chief','Nathan Drake','The Witcher','Garfield','Duke Nukem','Zelda'],
    ['\'s ',' and the ',': ',': The ',' in the '],
    ['Epic ','Magical ','Lost ','Dark ','Sexy ','Dead ','Endless ','Legendary ','Chaos ','Final '],
    ['Adventure','Legend','Crystals','Warfare','World','Uprising','Shock','Fantasy','Souls','Doom'],
    ['',' DX',': Director\'s Cut',' Redux',' 2',' 3',': Legendary Edition',' GOTY',' Remastered',': Prepare to Die Edition']]

DESCRIPTORS = ['a great','an amazing','a classic','a new','an action-packed']

DISCOUNTS = [0,25,50,75]

TAGS = ['Indie','Action','Adventure','Strategy','Casual',
    'Simulation','RPG','Multiplayer','Sports','Racing',
    'Puzzle','Horror','Open World','Sci-Fi','Gambling',
    'Educational','FPS','TPS','Platformer','MMO']

generated_names = []

GAME_AMOUNT = 100
USER_AMOUNT = 1000

start_birthdate = date(1940, 1,1)
end_birthdate = date(2007, 1,1)
start_paymentdate = date(2017,1,1)
end_paymentdate = date(2017,2,23)

def gen_name():
    name = ''
    for i in NAMES:
        name += choice(i)
    return name

def gen_description(name):
    return name+' is '+choice(DESCRIPTORS)+' game.'

def gen_game(dev):
    g_name = gen_name()
    while g_name in generated_names:
        g_name = gen_name()
    generated_names.append(g_name)
    g_desc = gen_description(g_name)
    game = Game(name=g_name,
        developer=dev,
        url='http://www.game.com/'+g_name.replace(' ','_').replace('\'','_'),
        price=randint(1,100),
        discount=choice(DISCOUNTS),
        short_description=g_desc,
        long_description=g_desc+' This is a long description.')
    game.save()
    all_tags = Tag.objects.all()
    for tag in all_tags:
        if randint(0,3)==0:
            game.tags.add(tag)
    game.save()
    return game.pk

def gen_tags():
    for t in TAGS:
        tag = Tag(name=t)
        tag.save()

def gen_all(dev):
    Game.objects.all().delete()
    Tag.objects.all().delete()
    gen_tags()
    generated_names = []
    generated_games = []
    for i in range(GAME_AMOUNT):
        generated_games.append(gen_game(dev))
    return generated_games

def gen_random_date(start, end):
    return start + timedelta(seconds=randint(0, int((end-start).total_seconds())))

def gen_user(developer=False):
    name = uuid4() if not developer else 'dev1'
    email = '{0}@testland.com'.format(name)
    user = User.objects.create_user(username=name, password='salasana', email=email)
    pr = user.profile
    pr.email = email
    pr.birthdate = gen_random_date(start_birthdate, end_birthdate)
    pr.country = choice(COUNTRY_CODES)
    pr.phone = ''.join(sample(string.digits, 10))
    pr.zip = ''.join(sample(string.digits, 10))
    pr.save()
    if developer:
        developer = Developer.objects.create(user=user, developer_name = name, bank_account = uuid4(), company = name+".INC")
    return user, developer

def gen_payment(user, game):
    PaymentHistory.objects.create(
        user_id=user,
        game_id=game,
        payment_time = gen_random_date(start_paymentdate, end_paymentdate),
        amount = randint(1,100)
        )


def gen_owned_games(user, game):
    ow = OwnedGame.objects.create(
        user_id=user,
        game_id=game,
        time_played=randint(10,10000),
        rating = randint(0,5)
        )

def reset_tables(table):
    if table:
        table.delete()

def gen_tests_for_statistics():
    start = time.time()
    User.objects.all().delete()
    Profile.objects.all().delete()
    OwnedGame.objects.all().delete()
    Developer.objects.all().delete()
    users = []
    print("{} Creting a developer to own all games".format(time.time()-start))
    devuser, dev = gen_user(True)
    print("{} Creating games".format(time.time()-start))
    games = gen_all(dev)
    print("{} Creating users".format(time.time()-start))
    for i in range(USER_AMOUNT):
        if i % 10==0:
            print(i)
        user, temp = gen_user()
        users.append(user.pk)
        game = Game.objects.get(pk=choice(games))
        gen_payment(user, game)
        gen_owned_games(user, game)
    print("{} finished".format(time.time()-start))
    #games = Games.objects.filter(developer=None).update(developer=1)


if __name__=='__main__':
    gen_tests_for_statistics()


        
