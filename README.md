Kulavi Game Store
=================



A Django Game store site for playing Javascript Games. 
This is a Web Software Development course project from 2017.

Relates to GIS because it has a web map.

Try it from the ![demo site](https://kulavistore.herokuapp.com/)!

### Technologies used
* Django framework
* Python, Javascript and CSS
* Amazon S3 storage

### Gredits
* Joona - Site layout, Amazon S3 storing, Django apps login and devstatistics
* Sampo - Site layout, Kulavinlinna test game Django apps gamebrowser and restapi
* Vili - Site layout, Django app payments


![Front page](/kulavi/resources/media/frontpage.png) 

![Statistics ](/kulavi/resources/media/staistics1.png)