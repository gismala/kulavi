//Kulavinlinna: WSD 16-17 Project Game

$(document).ready( function() {
  //Show only data check when arriving on page
  $("#dataCheck").show();
  $("#game").hide();

  //Resources
  var exploreBG = "exploreBG.png";
  var battleBG = "battleBG.png";

  var monsters = [
    {
      name: "Rat",
      hp: 8,
      attack: 4,
      image: "rat.png"
    },
    {
      name: "Furious Rat",
      hp: 8,
      attack: 14,
      image: "furiousrat.png"
    },
    {
      name: "Ghost",
      hp: 12,
      attack: 2,
      image: "ghost.png"
    },
    {
      name: "Skeleton",
      hp: 10,
      attack: 6,
      image: "skeleton.png"
    },
    {
      name: "Armored Skeleton",
      hp: 15,
      attack: 6,
      image: "armoredskeleton.png"
    },
    {
      name: "Goblin",
      hp: 25,
      attack: 4,
      image: "goblin.png"
    }
  ];
  var itemTypes = [
    {
      name: "Health Potion"
    },
    {
      name: "Fire Potion"
    }
  ];
  var colors = {
    normal: "black",
    monster: "darkRed",
    well: "lightBlue",
    exit: "gold"
  };

  //Game progress variables
  var score;
  var level;
  var room;
  var hp;
  var items;
  var battle;
  var seenWell;
  var usedWell;

  //Explore variables
  var doors;

  //Battle variables
  var itemsOpen;
  var playerTurn;
  var monsterType;
  var monsterHp;
  var defending;
  var monsterCharging;

  //Generate random doors
  function generateRooms() {
    doors = [];

    var normalChance = 100; //Base chance
    var monsterChance = 70 + 20 * level; //Based on level: 80, 90, 100...
    var exitThreshold = 15 + 5 * level; //Exit can't spawn before this room
    var seenExit = false; //Only applies to this room, not whole floor

    //Main calculation
    for(i=0; i<3; i++) {
      var exitChance = 0;
      var wellChance = 0;
      if(room>=exitThreshold && !seenExit) { //Exit has a chance of spawning after threshold
        exitChance = 30;
      }
      if(room>=exitThreshold-10 && !seenWell) { //Well can't spawn before ET-10
        wellChance = 20;
      }

      var r = Math.random() * (normalChance+monsterChance+wellChance+exitChance);
      if(r<normalChance) {
        doors[i] = "normal";
      }
      else if(r<normalChance+monsterChance) {
        doors[i] = "monster";
      }
      else if(r<normalChance+monsterChance+wellChance) {
        doors[i] = "well";
        seenWell = true;
      }
      else if(r<normalChance+monsterChance+wellChance+exitChance) {
        doors[i] = "exit";
        seenExit = true;
      }
    }

    //If player is at the first exit, ensure that one of the doors is an exit.
    //In subsequent rooms, there is only a chance for an exit to spawn.
    if(room===exitThreshold && !($.inArray("exit",doors)>-1)) {
      doors[1] = "exit";
    }
  }

  //For (re)starting the game
  function initialize() {
    score = 0;
    level = 1;
    room = 1;
    hp = 100;
    items = [];
    battle = false;
    seenWell = false;
    usedWell = false;
    generateRooms();
    initExplore();
    updateHud();

    dispMessage("Step into the Kulavinlinna!");
  }

  //For starting a battle
  function initBattle() {
    battle = true;
    itemsOpen = false;
    playerTurn = true;
    defending = false;
    monsterType = Math.floor(Math.random() * monsters.length);
    monsterHp = monsters[monsterType].hp;
    monsterCharging = false;

    updateHud();
    dispMessage("You encountered " + monsters[monsterType].name + "!");
  }

  //For starting exploration (after battle)
  function initExplore() {
    battle = false;

    updateHud();
  }

  //When a random event occurs
  function doEvent() {

    var damageChance = 100;
    var goldChance = 20;
    var itemChance = 80;
    if(room > 15 + 5 * level) { //This is actually exitThreshold
      itemChance = 0;
    }

    var r = Math.random() * (damageChance+goldChance+itemChance);
    if(r<damageChance) {
      var dmg = Math.floor(Math.random() * 6) + 5;
      hp = Math.max(0, hp - dmg); //Damage between 5 and 10
      updateHud();
      dispMessage("A brick fell on you and did " + dmg + " damage!");
    }
    else if(r<damageChance+goldChance) {
      var gold = Math.floor(Math.random() * 21) + 10;
      score += gold; //Score between 10 and 30
      updateHud();
      dispMessage("You found " + gold + " gold!");
    }
    else if(r<damageChance+goldChance+itemChance) {
      var itemType = Math.floor(Math.random() * itemTypes.length);
      items.push(itemType);
      updateHud();
      dispMessage("You found a " + itemTypes[itemType].name + "!");
    }
  }

  //For using the HP regenerating well
  function useWell() {
    usedWell = true;
    hp = 100;

    updateHud();
    dispMessage("You drank from the magical well and it restored your HP! You also feel cursed!");
  }

  //For when player exits the level
  function endLevel() {
    level += 1;
    room = 1;
    hp = 100;
    seenWell = false;
    usedWell = false;
    initExplore();
    updateHud();
    dispMessage("Level complete!");
  }

  //When selecting one of the doors
  function enterRoom(roomType) {
    room += 1;
    switch(roomType) {
      case "normal":
        if(Math.floor(Math.random() * 2)===1) {
          doEvent();
        }
        break;
      case "monster":
        initBattle();
        break;
      case "well":
        useWell();
        break;
      case "exit":
        endLevel();
        break;
    }
    generateRooms();
    updateHud();
  }

  //Battle actions
  function attack() {
    playerTurn = false;
    var dmg = Math.floor(Math.random() * 6) + 5; //Do damage between 5 and 10
    monsterHp = Math.max(0, monsterHp - dmg);

    updateHud();
    dispMessage("You attack and deal " + dmg + " damage!");
    monsterTurn();
  }

  function defend() {
    playerTurn = false;
    defending = true;

    updateHud();
    monsterTurn();
  }

  function openItems() {
    itemsOpen = true;
    updateHud();
  }

  function closeItems() {
    itemsOpen = false;
    updateHud();
  }

  function useItem(itemIndex) {
    playerTurn = false;
    itemsOpen = false;
    switch(items[itemIndex]) {
      case 0:
        hp = Math.min(100, hp+30);
        updateHud();
        dispMessage("You used the " + itemTypes[items[itemIndex]].name + " which heals 20 HP!");
        break;
      case 1:
        monsterHp = Math.max(0, monsterHp-20);
        updateHud();
        dispMessage("You used the " + itemTypes[items[itemIndex]].name + " which deals 20 damage!");
        break;
    }
    items.splice(itemIndex, 1); //Remove the used item
    monsterTurn();
  }

  function monsterTurn() {
    //Check if the monster is dead
    if(monsterHp===0) {
      winBattle();
      return;
    }

    if(Math.floor(Math.random() * 4)===0 && !monsterCharging) { //25% chance that monster charges
      monsterCharging = true;
      updateHud();
      dispMessage(monsters[monsterType].name + " is charging!");
    }
    else {
      //Monster attacks, damage calculations
      var defendAmount = 0;
      var chargeMultiplier = 1;
      if(defending) {
        defendAmount = 10;
      }
      if(monsterCharging) {
        chargeMultiplier = 3;
      }
      var dmg = Math.max(0, (monsters[monsterType].attack + level) * chargeMultiplier - defendAmount);
      hp = Math.max(0, hp - dmg);
      monsterCharging = false;
      updateHud();
      dispMessage(monsters[monsterType].name + " attacks and deals " + dmg + " damage!");
    }

    if(hp===0) { //Player died
      death();
      return;
    }

    defending = false;
    playerTurn = true;
  }

  function winBattle() {
    if(!usedWell) { //You get no gold if you used a well
      var gold = Math.floor(Math.random() * 41) + 80;
      score += gold;
      updateHud();
      dispMessage("You won and gained " + gold + " gold!");
    }
    else {
      updateHud();
      dispMessage("You won!");
    }
    initExplore();
  }

  function death() {
    //Just shows your score and resets the game and sends a score message
    msgScore();
    dispMessage("You died! Score: " + score);
    initialize();
  }

  function dispMessage(messageText) {
    alert(messageText);
  }

  //Bind the functions to buttons on the page
  $("#left").click(function() {
    if(!battle) {
      enterRoom(doors[0]);
      msgSave();
    }
  });

  $("#middle").click(function() {
    if(!battle) {
      enterRoom(doors[1]);
      msgSave();
    }
  });

  $("#right").click(function() {
    if(!battle) {
      enterRoom(doors[2]);
      msgSave();
    }
  });

  $("#attack").click(function() {
    if(battle && playerTurn) {
      closeItems();
      attack();
      msgSave();
    }
  });

  $("#defend").click(function() {
    if(battle && playerTurn && !itemsOpen) {
      closeItems();
      defend();
      msgSave();
    }
  });

  $("#items").click(function() {
    if(battle && playerTurn) {
      if(itemsOpen) {
        closeItems();
      }
      else {
        openItems();
      }
    }
  });

  $(document).on("click", ".itemListItem", function() {
    if(battle && playerTurn && itemsOpen) {
      useItem($(this).index());
      msgSave();
    }
  });

  //Update the game view when something changes
  function updateHud() {
    $("#hudHp").html("HP: " + hp + "/100");
    $("#hudScore").html("Score: " + score);
    $("#hudLevel").html("Level: " + level);
    $("#hudRoom").html("Room: " + room);
    if(battle) {
      $("#phaseBattle").show();
      $("#phaseExplore").hide();
      $("#bg").css("background-image", "url(images/" + battleBG + ")");
      $("#monsterPic").css("background-image", "url(images/monster/" + monsters[monsterType].image + ")");
      $("#monsterName").html(monsters[monsterType].name);
      $("#monsterHp").html("HP: " + monsterHp);
      if(monsterCharging) {
        $("#monsterCharging").show();
      }
      else {
        $("#monsterCharging").hide();
      }
      if(itemsOpen) {
        $("#itemList").show();
        var itemTemp = [];
        $.each(items, function(i, item) {
          itemTemp.push("<li class=\"itemListItem\"><a href=\"#\">" + itemTypes[item].name + "</a></li>");
        });
        $('#itemListList').html(itemTemp.join(''));
      }
      else {
        $("#itemList").hide();
      }
    }
    else {
      $("#phaseBattle").hide();
      $("#phaseExplore").show();
      $("#bg").css("background-image", "url(images/" + exploreBG + ")");
      $("#doorLeft").css("background-color", colors[doors[0]]);
      $("#doorMiddle").css("background-color", colors[doors[1]]);
      $("#doorRight").css("background-color", colors[doors[2]]);
    }
  }

  //==========MESSAGING FUNCTIONS==========

  //Send score (on death)
  function msgScore () {
    var msg = {
      "messageType": "SCORE",
      "score": score
    };
    window.parent.postMessage(msg, "*");
  }

  //Save state
  function msgSave () {
    var msg = {
      "messageType": "SAVE",
      "gameState": {
        "score": score,
        "level": level,
        "room": room,
        "hp": hp,
        "items": items,
        "battle": battle,
        "seenWell": seenWell,
        "usedWell": usedWell,
        "doors": doors,
        "playerTurn": playerTurn,
        "monsterType": monsterType,
        "monsterHp": monsterHp,
        "defending": defending,
        "monsterCharging": monsterCharging
      }
    };
    window.parent.postMessage(msg, "*");
  }

  //Request save data
  function msgLoad () {
    var msg = {
      "messageType": "LOAD_REQUEST",
    };
    window.parent.postMessage(msg, "*");
  }

  //Restore game state according to save data
  window.addEventListener("message", function(evt) {
    if(evt.data.messageType === "LOAD") {
      console.log(evt.data);
      score = evt.data.gameState.score;
      level = evt.data.gameState.level;
      room = evt.data.gameState.room;
      hp = evt.data.gameState.hp;
      items = evt.data.gameState.items;
      battle = evt.data.gameState.battle;
      seenWell = evt.data.gameState.seenWell;
      usedWell = evt.data.gameState.usedWell;
      doors = evt.data.gameState.doors;
      playerTurn = evt.data.gameState.playerTurn;
      monsterType = evt.data.gameState.monsterType;
      monsterHp = evt.data.gameState.monsterHp;
      defending = evt.data.gameState.defending;
      monsterCharging = evt.data.gameState.monsterCharging;

      updateHud();
      $("#dataCheck").hide();
      $("#game").show();

      if(battle && !playerTurn) {
        monsterTurn();
      }
    }
    else if(evt.data.messageType === "ERROR") {
      alert(evt.data.info);
    }
  });

  //Request to set resolution to 600 x 373
  var message =  {
    messageType: "SETTING",
    options: {
      "width": 600,
      "height": 373
      }
  };
  window.parent.postMessage(message, "*");

  $("#newGame").click( function() {
    initialize();
    $("#dataCheck").hide();
    $("#game").show();
  });
  $("#loadGame").click( function() {
    msgLoad();
  });
});
