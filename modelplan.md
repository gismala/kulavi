### Game browser app

**Game**
name (charfield max, blank=false, unique=true)
url (urlfield, blank=false, unique=true)
price (positiveintegerfield, blank=false)
discount (positivesmallintegerfield, default=0)
developer (foreign, blank=false)
thumbnail (imagefield, default=default thumbnail)
short_description (charfield max)
long_description (textfield)
release_date (datetimefield, blank=false, auto_now_add=True)
update_date (datetimefield, blank=False, auto_now=True)
tags (manytomanyfield, related_name=games)

**Tag**
name (charfield 50, blank=false)

**Screenshots**
game_id (foreign, blank=false)
image (imagefield, blank=false)

### Login app

**User**
name (charfield max, blank=false)
first_name(charfield max)
last_name(charfield max)
email (emailfield, blank=false)
birthdate (datefield, blank=false)
phone (charfield 20)
country (charfield 2)
city (charfield max)
zip (charfield 20)
address1 (charfield max)
address2 (charfield max)
image (imagefield, default=default image)

**Developer**
user_id (foreign, blank=false)
developer_name (charfield max, blank=false)
company (charfield max)

### Game player app

**OwnedGame**
user_id (foreign)
game_id (foreign)
time_played (datetimefield)
score (positiveintegerfield)
rating (positivesmallintegerfield, blank=false)
state (textfield)

### Payments app

**PaymentHistory**
user_id (foreign)
game_id (foreign)
payment_time (datetimefield)
amount (positiveintegerfield)
payment_status (booleanfield)
